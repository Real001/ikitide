import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ApolloProvider} from 'react-apollo';
import {BrowserRouter} from 'react-router-dom';
import {renderRoutes} from 'react-router-config';
import {CookiesProvider} from 'react-cookie';
import createStore from './store';
import client from './apolloClient';
import routes from './UI/routes/routes';

const store = createStore(window.REDUX_STATE, {log: true});
const dom = (
  <CookiesProvider>
   <Provider store={store}>
    <ApolloProvider client={client}>
      <BrowserRouter>
        {renderRoutes(routes)}
      </BrowserRouter>
    </ApolloProvider>
   </Provider>
  </CookiesProvider>
);


ReactDOM.render(
dom,
    document.getElementById('app')
);
