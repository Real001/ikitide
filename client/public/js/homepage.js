
$(document).ready(function() {
    // МЕНЮ УПРАВЛЕНИЯ, СТИЛИЗАЦИЯ ДЛЯ ДОМАШНИЙ СТРАНИЦЫ
    let TOPHEIGHT, REALSCROLLSTART, autoNextActived, featuresExpanded;

    let hiding;
    const navbar = $("#navBg"); // Меню

    // Верхний текст заголовка
    const payoff = $(".payoff");
    const more = $(".more");

    // Верхняя карусель
    const header = $('#top-header');
    const pages = $('#carrousel .page');
    const buttons = $('#carrousel .top-carrousel-btn');
    const btnLeft = $("#top-carrousel-left");
    const btnRight = $("#top-carrousel-right");

    const screenshot = $(".screenshots");

    const featuresExpandButton = $("#features-extra-button");
    const featuresExpandPlusIcon = $("#features-extra-button img");
    const features = $("#features");
    const initialMaxHeight = "924px";

    featuresExpandButton.click(toggleExtraFeatures);
    $(".feature").click(function(n){
        $(this).find("a")[0].click();
    });


    let topOfPayOff = payoff.position().top + 140;
    let topOfScreenshot = screenshot.position().top;
    let initialDiffPayTop = (topOfScreenshot - topOfPayOff);

    /**** Изменение размера ****/

    function resize(){
        TOPHEIGHT = Math.min(1024, window.innerHeight);
        REALSCROLLSTART = 302 - Math.max(0, (TOPHEIGHT - 814 - 110) / 2);

        $("#carrousel").css("height", TOPHEIGHT + "px");

        if (($(".top-header").height()/969) > ($(".top-header").width()/1999))
            $(".top-header")[0].style.backgroundSize = "auto " + ($(".top-header").height() + 100) + "px";
        else
            $(".top-header")[0].style.backgroundSize = $(".top-header").width() + "px auto";

        $("#features").css("background-size", window.innerWidth > 2000 ? "cover" : "");

        scroll();
    }

    $(window).resize(resize);
    resize();

    /**** Scrolling ****/

    function scroll(){
        let scrollTop = $(document).scrollTop();

        // Меню
        (function(){
            if (scrollTop < REALSCROLLSTART) {
                navbar.stop(); hiding = false;
                navbar.removeClass("navbar-scrolled");
                navbar.css("margin-top", "0px");
            }
            else if (scrollTop < 500) {
                navbar.stop(); hiding = false;
                navbar.removeClass("navbar-scrolled");
                navbar.css("margin-top", (REALSCROLLSTART - scrollTop) + "px");
            }
            else if (scrollTop < Math.min(600, TOPHEIGHT - 100)) {
                if (!hiding && !navbar.overlay) {
                    navbar.removeClass("navbar-scrolled");
                    navbar.css("margin-top", "-100px");
                }
            }
            else {
                if (!navbar.overlay) {
                    navbar.css("margin-top", "");
                    navbar.css("top", "-51px");
                    navbar.addClass("navbar-scrolled");
                    navbar.animate({ "top": "0" }, 200);
                    navbar.overlay = true;
                }
                return;
            }

            if (navbar.overlay && !hiding) {
                hiding = true;
                navbar.animate({ "top": "-51px" }, 200, function(){
                    navbar.css("top", "0");
                    navbar.css("margin-top", "-100px");
                    hiding = false;
                });
            }

            navbar.overlay = false;
        })();

        // Top Header Opacity
        let diffPayTop = (topOfScreenshot - topOfPayOff - scrollTop);
        // if scrolling approaches bottom of buttons, fade them out
        // gradually, until it reaches the top of the payoff
        if (scrollTop < REALSCROLLSTART) {
            if (scrollTop > 0) {
                let opacity = 1- ((initialDiffPayTop - diffPayTop)
                    / initialDiffPayTop);

                if (opacity <= 1) {
                    more.css({ opacity: opacity });
                    payoff.css({ opacity: opacity });
                }
            }
            else {
                more.css({ opacity: 1 });
                payoff.css({ opacity: 1 });
            }
        }

        // Скриншот и заголовок
        if (scrollTop <= REALSCROLLSTART + TOPHEIGHT) {
            (function() {
                let yPos = -1 * (scrollTop / 12);
                //Установка последнее исходное положение для прокрутки parralax
                let coords = 'left ' + yPos + 'px';
                header.css({ "background-position": coords });

                // Покажите больше фона, пока не появится x% содержимого ниже него, затем прокрутите
                let minHeight = TOPHEIGHT + scrollTop;
                //Снимите верхний заголовок в нижней части экрана (до максимального размера)
                header.css("min-height", minHeight);
                pages.css("top", (scrollTop > 0 ? scrollTop : 0) + "px");
                buttons.css("margin-top", (scrollTop/2) + "px");

                //Исправьте скриншот в позиции около 100 пикселей, затем прокрутите
                if (scrollTop >= REALSCROLLSTART) {
                    // var currScrollTop = scrollTop;
                    header.css({
                        "min-height": TOPHEIGHT + REALSCROLLSTART
                    });
                    screenshot.css({"transform": "translate(0," + (-1 * scrollTop + ((scrollTop - REALSCROLLSTART) / 5)) + "px)" });
                }
                if (scrollTop < 290)
                    screenshot.css({"transform": "translate(0, " + (-1 * scrollTop) + "px)" });
            })();
        }
        if (!featuresExpanded && scrollTop > 1850 + TOPHEIGHT) {
            toggleExtraFeatures();
            featuresExpanded = true;
        }

        let yPos, coords;

        yPos = -1 * (scrollTop / 7) + 300;
        //Установите последнее исходное положение для прокрутки параклакса
        coords = 'left ' + yPos + 'px';
        $('#collaboration').css({ "background-position": coords });

        yPos = -1 * (scrollTop / 5) + 250; // -500 +
        // Установите последнее исходное положение для прокрутки параклакса
        coords = '50% ' + yPos + 'px';
        $('#features').css({ "background-position": coords });
    }
    $(document).scroll(scroll);

    //Подробнее нажмите кнопку «Подробнее», чтобы перейти к разделу «Рабочие области»
    function scrollToAnchor(aid){
        const aTag = $("a[name='"+ aid +"']");
        $('html,body').animate({
            scrollTop: aTag.offset().top + REALSCROLLSTART - 50
        },'slow');
    }

    $("#learnMore").click(function(event) {
        event.preventDefault();
        scrollToAnchor("workspaces");
    });

    /**** Carrousel ****/

    $("#carrousel .browser-frame").click(function(){
        body.animate({
            scrollTop: document.body.scrollTop ? 0 : REALSCROLLSTART
        }, 200);
    })

    const body = $("html, body");
    let currentPage = 1;
    let topCarrouselTimer;
    let animating = false;
    const AUTOINTERVAL = 8000;

    btnLeft.click(function(){
        let curPage = $("#carrousel-page" + currentPage);
        let nextPage = $("#carrousel-page" + --currentPage);
        if (!nextPage.length)
            nextPage = $("#carrousel-page" + (currentPage = 5));

        if (document.body.scrollTop) {
            body.animate({ scrollTop: 0 }, 200, function(){
                slide(curPage, nextPage);
            });
        }
        else {
            slide(curPage, nextPage);
        }
    });
    btnRight.click(function(){
        const curPage = $("#carrousel-page" + currentPage);
        let nextPage = $("#carrousel-page" + ++currentPage);
        if (!nextPage.length)
            nextPage = $("#carrousel-page" + (currentPage = 1));

        if (document.body.scrollTop) {
            body.animate({ scrollTop: 0 }, 200, function(){
                slide(curPage, nextPage, true);
            });
        }
        else {
            slide(curPage, nextPage, true);
        }
    });

    function slide(curPage, nextPage, fromLeft){
        if (animating) return;

        animating = true;
        clearTimeout(topCarrouselTimer);

        nextPage.css("margin-left", (fromLeft ? "" : "-") + window.innerWidth + "px");
        nextPage.addClass("active");

        curPage.animate({ "margin-left": (fromLeft ? "-" : "") + window.innerWidth + "px" }, 500, "easeInOutQuad");
        nextPage.animate({ "margin-left": "0" }, 500, "easeInOutQuad", function(){
            curPage.removeClass("active");

            animating = false;
            topCarrouselTimer = setTimeout(nextTopCarrousel, AUTOINTERVAL);
        });
    }

    function nextTopCarrousel(){
        const curPage = $("#carrousel-page" + currentPage);
        let nextPage = $("#carrousel-page" + ++currentPage);
        if (!nextPage.length)
            nextPage = $("#carrousel-page" + (currentPage = 1));

        slide(curPage, nextPage, true, function(){
            topCarrouselTimer = setTimeout(nextTopCarrousel, AUTOINTERVAL);
        });
    }

    topCarrouselTimer = setTimeout(nextTopCarrousel, AUTOINTERVAL);

    /**** Collaboration ****/

    //мигать курсором
    blink();
    function blink() {
        $("#cursor_typing").fadeTo(100, 0, function(){
            setTimeout(function(){
                $("#cursor_typing").fadeTo(100, 1.0,
                    setTimeout.bind(null, blink, 400));
            }, 400);
        });
    }
    blink_terminal();
    function blink_terminal() {
        $(".sudoblock .cursor").fadeTo(100, 0, function(){
            setTimeout(function(){
                $(".sudoblock .cursor").fadeTo(100, 1.0,
                    setTimeout.bind(null, blink_terminal, 400));
            }, 400);
        });
    }

    /**** Extra Features ****/

    function toggleExtraFeatures() {
        let maxHeight = "1588px";

        if (features.css("max-height") == maxHeight) {
            maxHeight = initialMaxHeight;
            features.removeClass("expanded");
        }
        else {
            features.addClass("expanded");
        }

        features.animate({"max-height": maxHeight}, 500, "easeInOutQuad");
    }




    resize();
});