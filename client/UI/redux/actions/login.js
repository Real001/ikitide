export const SET_USER = 'SET_USER';
export function setInitial(value) {
  return {
    type: SET_USER,
    value
  };
}