export const SET_DATA_TASK = 'SET_DATA_TASK';
export const SET_DATA_ACADEMIC_SUBJECT = 'SET_DATA_ACADEMIC_SUBJECT';
export const SET_DATA_CLASSES = 'SET_DATA_CLASSES';
export const SET_DATA_GROUPS = 'SET_DATA_GROUPS';
export const SET_DATA_START_TASK = 'SET_DATA_START_TASK';
export const SET_DATA_START_LESSON = 'SET_DATA_START_LESSON';


export function setInitialDataTask(value) {
  return {
    type: SET_DATA_TASK,
    value
  }
}

export function setInitialDataAcademicSubject(value) {
  return {
    type: SET_DATA_ACADEMIC_SUBJECT,
    value
  }
}

export function setInitialDataClasses(value) {
  return {
    type: SET_DATA_CLASSES,
    value
  }
}
export function setInitialDataGroups(value) {
  return {
    type: SET_DATA_GROUPS,
    value
  }
}
export function setInitialDataStartTask(value) {
  return {
    type: SET_DATA_START_TASK,
    value
  }
}

export function setInitialDataStartLesson(value) {
  return {
    type: SET_DATA_START_LESSON,
    value
  }
}