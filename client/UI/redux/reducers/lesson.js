import update from 'immutability-helper';
import {SET_DATA_TASK, SET_DATA_ACADEMIC_SUBJECT, SET_DATA_CLASSES, SET_DATA_GROUPS, SET_DATA_START_TASK, SET_DATA_START_LESSON} from "../actions/lesson"

const ininitalState = {
  taskId: null,
  academicSubjectId: null,
  classesId: null,
  groups:[],
  startTask: null,
  startLesson: null
};

export default (state = ininitalState, action) => {
  switch (action.type) {
    case SET_DATA_TASK: {
      return update(state, {taskId: {$set: action.value}});
    }
    case SET_DATA_ACADEMIC_SUBJECT: {
      return update(state, {academicSubjectId: {$set: action.value}});
    }
    case SET_DATA_CLASSES: {
      return update(state, {classesId: {$set: action.value}});
    }
    case SET_DATA_GROUPS: {
      return update(state, {groups: {$push: [action.value]}});
    }
    case SET_DATA_START_TASK: {
      return update(state, {startTask: {$set: action.value}});
    }
    case SET_DATA_START_LESSON: {
      return update(state, {startLesson: {$set: action.value}});
    }
    default: return state;
  }
};