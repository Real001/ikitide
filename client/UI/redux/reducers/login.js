import update from 'immutability-helper';
import {SET_USER} from "../actions/login";

const initialState = {
  token: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER: {
      return update(state, {
        token: {$set: action.value}
      });
    }
    default:
      return state;
  }
}