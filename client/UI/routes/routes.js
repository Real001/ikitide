import IDE from '../pages/ide/IDEContainer';
import Login from '../pages/login/login';
import MainTeacher from '../pages/MainTeacher';
import Desktop from '../pages/desktop/Desktop';
import Classes from '../pages/teacher/classes/Classes';
import ClassesEdit from '../pages/teacher/classes/ClassesEdit';
import ClassesAdd from '../pages/teacher/classes/ClassesAdd';
import Tasks from '../pages/teacher/tasks/Tasks';
import TaskAdd from '../pages/teacher/tasks/TaskAdd';
import TaskEdit from '../pages/teacher/tasks/TaskEdit'
import Students from '../pages/teacher/students/Students';
import Statistics from '../pages/teacher/statistics/Statistics';
import ClassesInfo from '../pages/teacher/classes/ClassesInfo';
import TaskInfo from '../pages/teacher/tasks/TaskInfo';
import StudentInfo from '../pages/teacher/students/StudentInfo';
import LessonPage from '../pages/teacher/lesson/LessonPage';
import СonductingLesson from '../pages/teacher/lesson/СonductingLesson';
import ViewLesson from '../pages/student/lesson/View';
import ProfileStudent from '../pages/student/profile/Profile';

export default [
  {
    path: '/login',
    component: Login
  },
  {
    path: '/ide',
    component: IDE
  },{
  // routes : [
  //   {
  //     path: '/ide',
  //     exact: true,
  //     component: App
  //   },
  //   {
  //     path: '/login',
  //     component: Login
  //   },

  path: '/teacher',
  component: MainTeacher,
  routes: [
    {
      path: '/teacher/desktop',
      component: Desktop,
    },
    {
      path: '/teacher/classes/edit/:id',
      component: ClassesEdit,
    },
    {
      path: '/teacher/classes/add',
      component: ClassesAdd,
    },
    {
      path: '/teacher/classes/view/:id',
      component: ClassesInfo,
    },
    {
      path: '/teacher/classes',
      component: Classes,
    },
    {
      path: '/teacher/tasks',
      component: Tasks,
    },
    {
      path: '/teacher/task/add',
      component: TaskAdd,
    },
    {
      path: '/teacher/task/edit/:id',
      component: TaskEdit,
    },
    {
      path: '/teacher/task/view/:id',
      component: TaskInfo,
    },
    {
      path: '/teacher/students',
      component: Students,
    },
    {
      path: '/teacher/student/view/:id',
      component: StudentInfo,
    },
    {
      path: '/teacher/statistics',
      component: Statistics,
    },
    {
      path: '/teacher/start-lesson',
      component: LessonPage,
    },
    {
      path: '/teacher/lesson',
      component: СonductingLesson,
    },
  ],


  // ]
}, {
    path: '/student',
    component: MainTeacher,
    routes: [
      {
        path: '/student/desktop',
        component: Desktop,
      },
      {
        path: '/student/lesson/:id',
        component: ViewLesson,
      },
      {
        path: '/student/profile/',
        component: ProfileStudent,
      },
    ]
  }]