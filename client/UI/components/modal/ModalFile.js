import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import ReactModal from 'react-modal';

export class ModalFile extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.object,
    isShowModal: PropTypes.bool,
    onHandleCloseModal: PropTypes.func
  };

  static defaultProps = {
    children: {},
    isShowModal: false,
    onHandleCloseModal: () => {}
  };

  constructor(props) {
    super(props);

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal(e) {
    e.preventDefault();
    this.props.onHandleCloseModal(true);
  }

  handleCloseModal(e) {
    e.preventDefault();
    this.props.onHandleCloseModal(false);
  }

  render() {
    return (
      <ReactModal
        isOpen={this.props.isShowModal}
        contentLabel="Modal"
        onRequestClose={this.handleCloseModal}
      >
        {this.props.children}
      </ReactModal>
    )
  }
}