import DropDownIcon from 'material-design-icons/navigation/svg/production/ic_arrow_drop_down_18px.svg?';
import DropDownWhiteIcon from 'material-design-icons/navigation/svg/production/ic_arrow_drop_down_18px.svg?fill=white';
import MenuIcon from 'material-design-icons/navigation/svg/production/ic_menu_18px.svg?fill=white';
import HelpIcon from 'material-design-icons/action/svg/production/ic_help_24px.svg?fill=white';
import RefreshIcon from 'material-design-icons/action/svg/production/ic_autorenew_24px.svg?fill=white';
import EditIcon from 'material-design-icons/image/svg/production/ic_edit_24px.svg?fill=white';
import SaveIcon from 'material-design-icons/content/svg/production/ic_save_24px.svg?fill=white';
import SearchIcon from 'material-design-icons/action/svg/production/ic_search_24px.svg';
import AddIcon from 'material-design-icons/content/svg/production/ic_add_48px.svg?fill=white';
import MoreIcon from 'material-design-icons/navigation/svg/production/ic_more_vert_24px.svg';
import MoreIconWhite from 'material-design-icons/navigation/svg/production/ic_more_vert_24px.svg?fill=white';
import FilterIcon from 'material-design-icons/image/svg/production/ic_tune_24px.svg?fill=#3f8cf0';
import ArrowRightIcon from 'material-design-icons/hardware/svg/production/ic_keyboard_arrow_right_24px.svg?fill=#3f8cf0';
import DeleteIcon from 'material-design-icons/action/svg/production/ic_delete_24px.svg?fill=#de3d53';
import LessIcon from 'material-design-icons/navigation/svg/production/ic_unfold_less_24px.svg?fill=#3f8cf0';
import FirstPageIcon from 'material-design-icons/navigation/svg/production/ic_first_page_24px.svg';
import LastPageIcon from 'material-design-icons/navigation/svg/production/ic_last_page_24px.svg';
import LeftIcon from 'material-design-icons/navigation/svg/production/ic_chevron_left_24px.svg';
import RightIcon from 'material-design-icons/navigation/svg/production/ic_chevron_right_24px.svg';
import CalendarIcon from 'material-design-icons/action/svg/production/ic_today_24px.svg';
import SearchIconWhite from 'material-design-icons/action/svg/production/ic_search_24px.svg?fill=#ffffff';
import LocationIcon from 'material-design-icons/maps/svg/production/ic_my_location_24px.svg';
import DateRangeIcon from 'material-design-icons/action/svg/production/ic_date_range_24px.svg';
import WorkIcon from 'material-design-icons/action/svg/production/ic_work_24px.svg';
import CompanyIcon from 'material-design-icons/places/svg/production/ic_business_center_24px.svg';
import MapIcon from 'material-design-icons/communication/svg/production/ic_location_on_24px.svg';
import PersonIcon from 'material-design-icons/social/svg/production/ic_person_24px.svg';
import KeyIcon from 'material-design-icons/communication/svg/production/ic_vpn_key_24px.svg';
import PhoneIcon from 'material-design-icons/maps/svg/production/ic_local_phone_24px.svg';
import EditIconGray from 'material-design-icons/image/svg/production/ic_edit_24px.svg';
import DeleteIconGray from 'material-design-icons/action/svg/production/ic_delete_24px.svg';
import DeleteIconRed from 'material-design-icons/action/svg/production/ic_delete_24px.svg?fill=#de3d53';
import DescriptionIcon from 'material-design-icons/action/svg/production/ic_description_24px.svg';
import ErrorIcon from 'material-design-icons/alert/svg/production/ic_error_outline_24px.svg';
import DuplicateIcon from 'material-design-icons/content/svg/production/ic_content_copy_24px.svg';
import ClearIcon from 'material-design-icons/content/svg/production/ic_clear_24px.svg';
import ClearIconWhite from 'material-design-icons/content/svg/production/ic_clear_24px.svg?fill=white';
import ArrowDown from 'material-design-icons/navigation/svg/production/ic_arrow_downward_24px.svg';
import ArrowUp from 'material-design-icons/navigation/svg/production/ic_arrow_upward_24px.svg';

export {
  DropDownIcon,
  DropDownWhiteIcon,
  MenuIcon,
  HelpIcon,
  RefreshIcon,
  EditIcon,
  SaveIcon,
  SearchIcon,
  AddIcon,
  MoreIcon,
  MoreIconWhite,
  FilterIcon,
  ArrowRightIcon,
  DeleteIcon,
  LessIcon,
  FirstPageIcon,
  LastPageIcon,
  RightIcon,
  LeftIcon,
  CalendarIcon,
  SearchIconWhite,
  LocationIcon,
  DateRangeIcon,
  WorkIcon,
  CompanyIcon,
  MapIcon,
  PersonIcon,
  KeyIcon,
  PhoneIcon,
  EditIconGray,
  DeleteIconGray,
  DeleteIconRed,
  DescriptionIcon,
  ErrorIcon,
  DuplicateIcon,
  ClearIcon,
  ClearIconWhite,
  ArrowDown,
  ArrowUp
};