import React from 'react';
import TextEditor from './TextEditor';

export default function renderFieldEditorText({input}) {
return (
  <TextEditor
    input={input}
  />
)
}