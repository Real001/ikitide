import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class TheField extends Component {
  static propTypes = {
    input: PropTypes.string
  };

  constructor(props) {
    super(props);
    const editorState = this.initEditorState();
    this.state = {
      editorState
    };
    this.changeValue(editorState);
  }

  /**
   * Initialising the value for <Editor />
   */
  initEditorState() {
    const html = '<div>Your content</div>';
    const contentBlock = htmlToDraft(html);
    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
    return EditorState.createWithContent(contentState);
  }

  /**
   * This is used by <Editor /> to handle change
   */
  handleChange(editorState) {
    this.setState({editorState});
    this.changeValue(editorState);
  }

  /**
   * This updates the redux-form wrapper
   */
  changeValue(editorState) {
    const value = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    this.props.input.onChange(value);
  }

  render() {
    const { editorState } = this.state;
    return (
        <Editor
          editorState={editorState}
          wrapperClassName="wrapper-class"
          editorClassName="editor-class"
          toolbarClassName="toolbar-class"
          onEditorStateChange={(editorState) => this.handleChange(editorState)}/>
    );

  }
}

export default TheField;