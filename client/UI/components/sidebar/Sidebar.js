import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {DropDownWhiteIcon} from '../Icons';
import {MenuLink} from "./MenuLink";
import MenuBlock from './MenuBlock'
import {menu, alternateMenuBlock} from './dataSidebarTeacher';
import '../../style/components/Sidebar/Sidebar.less'

class SidebarUserBtn extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    onClick: PropTypes.func
  };
  render() {
    return (
      <a className="sidebarUser" onClick={this.props.onClick}>
        {this.props.title}
        {/*<DropDownWhiteIcon />*/}
      </a>);
  }
}

class Sidebar extends PureComponent {
  static propTypes = {
    menuIsOpen: PropTypes.bool
  };

  static defaultProps = {
    menuIsOpen: false
  };

  constructor(props) {
    super(props);
    this.state = {
      menuPersonalInfo: false,
    };

    this.handleChangeMenu = this.handleChangeMenu.bind(this);
  }

  handleChangeMenu() {
    this.setState({ menuPersonalInfo: !this.state.menuPersonalInfo });
  }

  render() {
    const alternateMenu = (
      <div>
        {alternateMenuBlock.map(ml => <MenuLink route={ml.route} title={ml.title} />)}
        <div className="hr" />
        <MenuLink
          title="Выход"
          onClick={() => {}}
          route="/"
        />
      </div>);
    return (
      <div className="sidebarBase">
        <div className="sidebarHead">
          <p className="sidebarBrand">Преподаватель</p>
           <SidebarUserBtn title="Легалов Александр Иванович" onClick={this.handleChangeMenu} />
        </div>
        {this.state.menuPersonalInfo ?
          alternateMenu :
          menu.map(mb => <MenuBlock title={mb.title} links={mb.menuLinks} />)}
      </div>);
  }
}

const mapStateToProps = state => ({
  // menuIsOpen: state.pages.main.menuIsOpen
});

export default connect(mapStateToProps)(Sidebar);