import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import People from 'material-ui/svg-icons/social/people';
import Bookmark from 'material-ui/svg-icons/image/collections-bookmark';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Web from 'material-ui/svg-icons/av/web';
import {cyan600, pink600, purple600} from 'material-ui/styles/colors';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';


const menu = [{
  title: 'Занятия',
  menuLinks: [
    {route: '/teacher/lecture', title: 'Занятия'},
    {route: '/teacher/lecture', title: 'Задания'}
  ]
}, {
  title: 'Студенты',
  menuLinks: [
    {route: '/teacher/lecture', title: 'Список студентов'},
    {route: '/teacher/lecture', title: 'Статистика'}
  ]
}];

const alternateMenuBlock = [
  {route: '/teacher/account', title: 'Личный кабинет'},
  {route: '/teacher/my-group', title: 'Мои группы'}];

const menus = {
  teacher: [
    { text: 'Занятия', icon: <Bookmark/>, link: '/teacher/classes' },
    { text: 'Задачи', icon: <Web/>, link: '/teacher/tasks' },
    { text: 'Студенты', icon: <People/>, link: '/teacher/students' },
    { text: 'Статистика', icon: <Assessment/>, link: '/teacher/statistics' },
    { text: 'Проведение занятия', icon: <Assessment/>, link: '/teacher/start-lesson' },
    { text: 'IDE', icon: <Assessment/>, link: '/ide' }
  ],
  student: [
    { text: 'Занятия', icon: <Bookmark/>, link: '/student/lesson' },
    { text: 'Задачи', icon: <Web/>, link: '/student/tasks' },
    { text: 'IDE', icon: <Assessment/>, link: '/ide' }
  ]
};

export {
  menu,
  alternateMenuBlock,
  menus
};