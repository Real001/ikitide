import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {MenuLink, SubmenuLink} from "./MenuLink"

export default class MenuBlock extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    links: PropTypes.array,
  };
  static defaultProps = {
    title: '',
    links: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      height: 0
    };

    this.handleCloseSubmenu = this.handleCloseSubmenu.bind(this);
  }

  componentDidMount() {
    const height = this.testelem.clientHeight;
    this.state.height = height;
  }

  handleCloseSubmenu() {
    this.setState({ isOpen: !this.state.isOpen});
  }

  render() {
    return (
      <ul className={this.state.isOpen ? 'submenu-close' : ''}>
        <SubmenuLink type="button" title={this.props.title} onClick={this.handleCloseSubmenu} />
        <ul className="submenu" style={this.state.isOpen ? {height: '0'} : {height: this.state.height}}>
          <div
            ref={(elem) => { this.testelem = elem; }}
          >
            {this.props.links.map(ml => (
              <MenuLink route={ml.route} title={ml.title} />))
            }
          </div>
        </ul>
      </ul>);
  }

}