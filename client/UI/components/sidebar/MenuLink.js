import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import {DropDownIcon} from '../Icons';
import '../../style/components/Sidebar/MenuLink.less';

class MenuLink extends PureComponent {
  static propTypes = {
    route: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func
  };
  render() {
    return(
      <li className="lilink" onClick={this.props.onClick}>
        <div className="linkicon" />
        <Link className="linkbase link" to={this.props.route}>{this.props.title}</Link>
      </li>);
  }
}

class SubmenuLink extends PureComponent{
  static propTypes = {
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func
  };
  render() {
    return (
      <li className={'lilink submenu-link'} onClick={this.props.onClick}>
        <div className="titleicon iconbase" />
        <a className="submenu">
          {this.props.title}
        </a>
      </li>);
  }
}

SubmenuLink.defaultProps = {
  onClick: () => {}
};

export {
  MenuLink,
  SubmenuLink,
}