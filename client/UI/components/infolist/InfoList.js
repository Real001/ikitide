import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './style.less';

export class InfoListDt extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired
  };
  render() {
    return (
      <dt><span>{this.props.title}</span></dt>);
  }
}

export class InfoListDd extends PureComponent {
  static propTypes = {
    title: PropTypes.string
  };
  render() {
    return (
      <dd>{this.props.title ? this.props.title : '-'}</dd>);
  }
}
