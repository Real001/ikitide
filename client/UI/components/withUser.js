import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import {pipe} from 'ramda';

const withUser = (Component) => {
  const UserQuery = gql`
    query GetUser($token: String!) {
        users {
            byToken(token: $token) {
                _id
                lastName
                firstName
                middleName
                email
                login
                role
                learnGroup
            }
        }
    }
  `;
  const result = class extends React.Component {
    static propTypes = {
      data: PropTypes.object
    };

    static defaultProps = {
      data: {}
    };

    render() {
      if (this.props.data.loading) {
        return "Идет загрузка, подождите"
      }
      const User = this.props.data &&
        this.props.data.users &&
        this.props.data.users.byToken;
      return (
        <Component
          {...this.props}
          User={User}
        />
      );
    }
  };
  return pipe(graphql(UserQuery, {
    options: ownProps => (
      {
        variables:
          {token: localStorage.getItem('token') || '-'}
      })
  }))(result)
};

export default withUser;