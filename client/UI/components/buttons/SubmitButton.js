import PropTypes from 'prop-types';
import React from 'react';
const SubmitButton = ({title}) => (
  <button className="btn btn-lg btn-warning btn-block" type="submit">
    {title}
  </button>
);

SubmitButton.propTypes = {
  title: PropTypes.string.isRequired
};

export default SubmitButton;