import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  onClear: PropTypes.func,
  onCancel: PropTypes.func,
  submitting: PropTypes.bool,
};

const defaultProps = {
  onClear: () => {},
  onCancel: () => {},
  submitting: false
};

const FormButtonsModal = ({onClear, onCancel, submitting}) => (

  <div>
    <button disabled={submitting} type="submit" title="Add" />
    <button title="Cancel" onClick={onCancel} />
  </div>
);
FormButtonsModal.propTypes = propTypes;
FormButtonsModal.defaultProps = defaultProps;
export default FormButtonsModal;