import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

export default class FormInput extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    small: PropTypes.bool,
    required: PropTypes.bool,
    value: PropTypes.string,
    type: PropTypes.string,
    input: PropTypes.object,
    name: PropTypes.string,
    touched: PropTypes.string,
    error: PropTypes.string,
    fullWidth: PropTypes.bool
  };
  static defaultProps = {
    title: '',
    placeholder: '',
    small: false,
    required: false,
    value: '',
    type: 'button',
    input: {},
    name: '',
    fullWidth: false
  };
  render() {
    return (
      <div className="form-group">
        <TextField
          hintText={this.props.placeholder}
          floatingLabelText={this.props.placeholder}
          fullWidth={this.props.fullWidth}
          value={this.props.value}
          required={this.props.required}
        />
        {this.props.touched && (this.props.error && <span className="error">{this.props.error}</span>)}
      </div>);
  }
}