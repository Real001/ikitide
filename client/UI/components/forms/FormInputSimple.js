import React from 'react';
import PropTypes from 'prop-types';



const FormInputSimple = ({input, type, placeholder, name, required}) => (
  <input {...input} type={type} placeholder={placeholder} className="form-control top"/>
);

FormInputSimple.propTypes = {
  type: PropTypes.string.isRequired,
  input: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired

};

export default FormInputSimple;