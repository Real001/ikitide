import PropTypes  from 'prop-types';
import React from 'react';


const pLogin =({title}) => (
  <p className="text-muted text-center">
    {title}
  </p>
);
pLogin.propTypes = {
  title: PropTypes.string.isRequired
};

export default pLogin;

