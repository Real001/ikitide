import React, {PureComponent} from 'react';
import Select from 'react-select';
// import 'react-select/dist/react-select.css';
import PropTypes from 'prop-types';

export class FormSelect extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    value: PropTypes.string,
    disabled: PropTypes.bool
  };

  state = {
    selectedOption: this.props && this.props.options && this.props.options.find(o => o.value === this.props.value),
  };
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  };

  render() {
    return (

        <Select
          name={this.props.id}
          value={this.state.selectedOption}
          onChange={this.handleChange}
          clearable={false}
          autosize={false}
          placeholder={this.props.placeholder ? this.props.placeholder : 'Выберите из списка...'}
          noResultsText="Ничего не найдено"
          disabled={this.props.disabled}
          options={this.props.options}
        />

    );
  }
}