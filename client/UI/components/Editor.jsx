import React, {Component} from 'react';
import AceEditor from 'react-ace-editor';
import PropTypes from 'prop-types';
import brace from 'brace';
import 'brace/mode/c_cpp';
import 'brace/mode/javascript';
import 'brace/theme/monokai';
import '../style/Editor.css'

class CodeEditor extends Component {

  static propTypes = {
    input: PropTypes.object,
    mode: PropTypes.string,
    theme: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
  };

  static defaultProps = {
    input: {},
    name: '',
    mode: '',
    theme: '',
    value: '',
  };

  // onChange(newValue, e) {
  //   console.log(newValue, e);
  //
  //   const editor = this.ace.editor;
  //   // console.log(editor.getValue());
  // }

  onChange = (newValue) => {
    this.props.input.onChange(newValue);
  };


  render() {
    return (
     <AceEditor
       mode={this.props.mode}
       theme={this.props.theme}
       onChange={this.onChange}
       defaultValue={this.props.value}
       name="IDE"
       style={{height: '600px'}}
       setOptions={{
         enableBasicAutocompletion: true,
         enableLiveAutocompletion: true,
       }}
     />
    )
  }
}

export default CodeEditor;