import React from 'react';
import FormInput from './forms/FormInput';
import CodeEditor from './Editor.jsx';

export function renderTextField({
                                    input, type, meta: { touched, error }, title, required, small, placeholder, fullWidth, value
                                  }) {
  return (
    <FormInput
      input={input}
      type={type}
      required={required}
      fullWidth={fullWidth}
      placeholder={placeholder}
      touched={touched}
      error={error}
    />
  );
}

export function renderCodeEditor({
  input, mode, theme, name
                                 }) {
  return (
    <CodeEditor
      input={input}
      mode={mode}
      theme={theme}
      name={input.name}
    />
  )
}