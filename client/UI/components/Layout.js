import styled from 'react-emotion';
import { css } from 'emotion';

export const Container = styled('div')`
  margin-left: 320px;
  display: flex;
  position: relative;
  height: 100vh;
  overflow-y: scroll;
  width: calc(100vw - 320px);
`;

export const ContainerContent = styled('div')`
  margin: 0 auto;
  margin-top: 80px;
  padding: 0 12px;
  min-width: 960px;
  width: 960px;
  
  @media (min-width: 1680px) {
    width: 1200px;
  }
`;

export const Row = styled('div')`
  margin-left: -12px;
  display: flex;
  flex-wrap: wrap;
`;

export const Col = css`
  position: relative;
  min-height: 1px;
  padding-left: 12px;
  padding-right: 12px;
`;

export const Col2 = styled('div')`
  ${Col};
  padding-right: 0px;
  width: 16.6666%;
`;

export const Col3 = styled('div')`
  ${Col};
  width: 25%;
`;


export const Col4 = styled('div')`
  ${Col};
  width: 33.3333%;
`;

export const Col6 = styled('div')`
  ${Col};
  width: 50%;
`;

export const Col8 = styled('div')`
  ${Col};
  width: 66.6666%;
`;

export const Col9 = styled('div')`
  ${Col};
  width: 75%;
`;


export const Col10 = styled('div')`
  ${Col};
  padding-left: 0px;
  padding-right: 0px;
  width: 83.3333%;
`;

export const Col12 = styled('div')`
  ${Col};
  padding-left: 0px;
   padding-right: 0px;
  width: 100%;
`;