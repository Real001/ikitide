import React from 'react';
import { Query } from "react-apollo";
import {Field} from 'redux-form';
import MenuItem from 'material-ui/MenuItem'
import {SelectField,} from 'redux-form-material-ui';
import {ClassesQuery} from "./queries"

const required = value => (value == null ? 'Required' : undefined);
const QueryClasses = () => (
  <Query query={ClassesQuery} variables={{offset:0, count: 10}}>
    {({loading, error, data}) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;
      return (
        <Field
          name="themeClass"
          hintText="Тема занятия"
          required
          type="text"
          component={SelectField}
          fullWidth
          validate={required}
        >
          {data && data.classes && data.classes.list.items.map(item => (
            <MenuItem value={item._id} primaryText={item.name} />
          ))}
        </Field>
      )
    }}
  </Query>
);

export default QueryClasses;