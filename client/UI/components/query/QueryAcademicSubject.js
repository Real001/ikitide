import React from 'react';
import { Query } from "react-apollo";
import {Field} from 'redux-form';
import MenuItem from 'material-ui/MenuItem'
import {SelectField,} from 'redux-form-material-ui';
import {GET_ACADEMIC_SUBJECT} from "./queries"

const required = value => (value == null ? 'Required' : undefined);
const QueryAcademicSubject = () => (

  <Query query={GET_ACADEMIC_SUBJECT} variables={{offset:0, count: 10}}>
    {({loading, error, data}) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;
      return (
        <Field
          name="academicSubject"
          hintText="Учебный предмет"
          required
          type="text"
          component={SelectField}
          fullWidth
          validate={required}
        >
          {data && data.academicSubjects && data.academicSubjects.list.items.map(item => (
            <MenuItem value={item._id} primaryText={item.title} />
          ))}
        </Field>
      )
    }}
  </Query>
);

export default QueryAcademicSubject;