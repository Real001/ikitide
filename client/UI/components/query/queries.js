import gql from 'graphql-tag';

export const GET_ACADEMIC_SUBJECT = gql`
    query GetAcademicSubject($offset: Int! $count: Int!)
    {
        academicSubjects{
            list(listOptions: {offset:$offset, count:$count}){
                items{
                    _id
                    title
                }
            }
        }
    }
`;

export const ClassesQuery = gql`
    query GetClasses($count: Int! $offset: Int!){
        classes {
            list(listOptions:{offset: $offset count:$count}){
                items{
                    _id
                    name
                }
            }
        }
    }
`;