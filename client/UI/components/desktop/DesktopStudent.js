import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment-timezone';


import './style.less';

const DesktopStudent = ({academicSubject, time, classes, onViewLesson}) => {
  return (
    <div className="base">
      <div className="info">
        <h4>{academicSubject}</h4>
        <p>Предмет</p>
      </div>
      <div className="info">
        <h4>{classes}</h4>
        <p>Тема занятия</p>
      </div>
      <div className="info">
        <h4>{moment(time).tz('Asia/Krasnoyarsk').format('DD.MM.YYYY HH:mm')}</h4>
        <p>Время начала</p>
      </div>
      <RaisedButton
        label={'Просмотр лекции'}
        primary={true}
        onClick={onViewLesson}
      />
    </div>
  )
};

export default DesktopStudent;