import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import {MenuIcon} from './Icons';
import '../style/components/Topbar.less'
import '../style/components/main/Body.less';
import '../style/components/Sidebar/MenuLink.less'

class MenuButton extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func,
    onPress: PropTypes.func
  };

  render() {
    return (
      <div className="menubutton" />
      )
  }
}

class Logout extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func
  };

  render() {
    return (
      <span className="linkbase" onClick={this.props.onClick}>
        <Link className="routelink" to="/">Выход</Link>
      </span>
    )
  }
}

class Topbar extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    menuIsOpen: PropTypes.bool,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    menuIsOpen: false,
    dispatch: () => {}
  };

  constructor() {
    super();
    this.handleSwitchMenu = this.handleSwitchMenu.bind(this);
  }

  handleSwitchMenu() {
    this.props.dispatch(changeMenuOpenStatus(!this.props.menuIsOpen));
  }

  render() {
    return(
      <div className="Topbarbase">
        <MenuButton onClick={this.handleSwitchMenu}/>
        <h1 className="H1">
          <span className="whitetext">{this.props.title}</span>
        </h1>
        <div className="rightcontent">
          <h3 className="H3">
            <span className="whitetext">{this.props.user}</span>
          </h3>
          <Logout/>
        </div>
      </div>
    )
  }
}

// const mapStateToProps = state => ({
//   menuIsOpen: state.pages.main.menuIsOpen
// });
//
// export default connect(mapStateToProps)(Topbar);

export default Topbar;
