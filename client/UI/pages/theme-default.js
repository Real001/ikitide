import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {orange600, grey900} from 'material-ui/styles/colors';

const themeDefault = getMuiTheme({
  palette: {
  },
  appBar: {
    height: 57,
    color: orange600
  },
  drawer: {
    width: 230,
    color: grey900
  },
  raisedButton: {
    primaryColor: orange600,
  },
  tabs: {
    backgroundColor: orange600
  }
});


export default themeDefault;