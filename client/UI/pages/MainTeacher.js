import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {renderRoutes} from 'react-router-config';
import {withRouter} from 'react-router';
import {withApollo} from 'react-apollo';
import {pipe} from 'ramda';
import DocumentTitle from 'react-document-title';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import withWidth, {LARGE, SMALL} from 'material-ui/utils/withWidth';

import Header from '../components/main/Header';
import LeftDrawer from '../components/main/LeftDrawer';
import ThemeDefault from './theme-default';
import {menus} from '../components/sidebar/dataSidebarTeacher';
import Tobbar from '../components/Topbar';
import DevTools from '../../DevTools';
import '../style/components/main/Body.less'
import '../style/components/main/Layout.less'
import withUser from '../components/withUser';

class MainTeacher extends Component {
  static propTypes = {
    location: PropTypes.shape,
    route: PropTypes.shape,
    children: PropTypes.element,
    width: PropTypes.number,
    User: PropTypes.object
  };
  static defaultProps = {
    data: {},
    location: {},
    route: {},
    User: null
  };

  constructor(props) {
    super(props);
    this.state = {
      navDrawerOpen: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.width !== nextProps.width) {
      this.setState({navDrawerOpen: nextProps.width === LARGE});
    }
  }

  handleChangeRequestNavDrawer() {
    this.setState({
      navDrawerOpen: !this.state.navDrawerOpen
    });
  }

  render() {
    let { navDrawerOpen } = this.state;
    const paddingLeftDrawerOpen = 236;

    const styles = {
      header: {
        paddingLeft: navDrawerOpen ? paddingLeftDrawerOpen : 0
      },
      container: {
        margin: '80px 20px 20px 15px',
        paddingLeft: navDrawerOpen && this.props.width !== SMALL ? paddingLeftDrawerOpen : 0
      }
    };

    return (
      <DocumentTitle title="Рабочий стол преподавателя">

        <MuiThemeProvider muiTheme={ThemeDefault}>
          <div>
            <Header styles={styles.header}
                    handleChangeRequestNavDrawer={this.handleChangeRequestNavDrawer.bind(this)} user={this.props.User}/>

            <LeftDrawer navDrawerOpen={navDrawerOpen}
                        menus={this.props.User && this.props.User.role === 'Студент' ? menus.student : menus.teacher}
                        User={this.props.User}
            />

            <div style={styles.container}>
              {/*{this.props.children}*/}

              {renderRoutes(this.props.route.routes)}
            </div>
          </div>
          {/*<DevTools/>*/}
        </MuiThemeProvider>

      </DocumentTitle>
    )
  }

}

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});


export default pipe(
  connect(mapStateToProps, mapDispatchToProps),
  withUser,
  withRouter,
  withApollo,
  withWidth()
)(MainTeacher);