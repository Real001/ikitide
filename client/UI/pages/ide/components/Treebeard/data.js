export const data= {
  name: 'lab1',
  toggle: true,
  children: [
    {
      name: 'include',
      children:[
        {name: 'prog.h'},
        {name: 'math.h'}
      ]
    },
    {
      name: 'assets',
      loading: true,
      children: []
    },
    {
      name: 'source',
      children: [
        {
          name: 'code',
          children: [
            {name: 'main.cpp'},
            {name: 'func.cpp'}
          ]
        }
      ]
    }
  ]
};