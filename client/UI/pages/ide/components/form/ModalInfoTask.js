import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Query} from "react-apollo";
import ThemeDefault from '../../../theme-default';
import {GET_LESSON, GET_TASK} from '../../../desktop/queries';
import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"

export default class DialogExampleModal extends React.Component {
  static propTypes = {
    openModal: PropTypes.bool,
    handleClose: PropTypes.func.isRequired
  };
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  // handleClose = () => {
  //   this.setState({open: false});
  // };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.props.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={true}
        onClick={this.handleClose}
      />,
    ];

    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <Query query={GET_LESSON} variables={{group: (this.props.student && this.props.student.learnGroup) ?
          this.props.student.learnGroup : ''}}>
          {({loading, error, data}) => {
            if (!loading) {
              return(
                <Query query={GET_TASK} variables={ {startLesson:  (data.startLessons &&
                  data.startLessons.byGroup && data.startLessons.byGroup) ? data.startLessons.byGroup._id : '' }}>
                  {({ loading, error, data }) => {
                    if (!loading) {
                      return ( <Dialog
                        title={data && data.executableTasks && data.executableTasks.byStartLesson && data.executableTasks.byStartLesson.task.name}
                        actions={actions}
                        modal={true}
                        open={this.props.openModal}
                        autoDetectWindowHeight={true}
                        autoScrollBodyContent={true}
                      >
                        <div>
                          <InfoListDt title="Описание"/>
                          <InfoListDd title={data && data.executableTasks && data.executableTasks.byStartLesson && data.executableTasks.byStartLesson.task.description}/>
                        </div>
                        <div>
                          <InfoListDt title="Входные данные"/>
                          <InfoListDd title={data && data.executableTasks && data.executableTasks.byStartLesson && data.executableTasks.byStartLesson.task.inputData}/>
                        </div>
                        <div>
                          <InfoListDt title="Выходные данные"/>
                          <InfoListDd title={data && data.executableTasks && data.executableTasks.byStartLesson && data.executableTasks.byStartLesson.task.outputData}/>
                        </div>
                      </Dialog>)
                    }
                    return null;
                  }}
                </Query>
              )
            }
            return null;
          }}
        </Query>
      </MuiThemeProvider>
    );
  }
}