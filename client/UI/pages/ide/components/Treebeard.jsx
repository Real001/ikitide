import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Treebeard} from 'react-treebeard';
import styled from 'react-emotion';
import style from '../../../components/treebeard/style';
import decorators from '../../../components/treebeard/decorators'

const TreeContainer = styled('div')`
  background-color: #f3f5f7 ;
  height: 600px;
`;

decorators.Header = ({style, node}) => {
  const iconType = node.children ? 'folder' : 'file-text';
  const iconClass = `fa fa-${iconType}`;
  const iconStyle = {marginRight: '5px'};

  return (
    <div style={style.base}>
      <div style={style.title}>
        <i className={iconClass} style={iconStyle}/>

        {node.name}
      </div>
    </div>
  );
};


class TreeExample extends Component {


  constructor(props) {
    super(props);
    this.state = {};
    this.onToggle = this.onToggle.bind(this);
  }

  onToggle(node, toggled) {
    if (this.state.cursor) {
      this.state.cursor.active = false;
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled
    }
    this.setState({cursor: node})
  }

  render() {
    return (
      <TreeContainer>
        <Treebeard
          style={style}
          data={this.props.data}
          onToggle={this.onToggle}
          decorators={decorators}

        />
      </TreeContainer>
    )
  }
}

export default TreeExample;