import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const EditMenu = (
    <SubMenu title={<span>Edit</span>}>
      <MenuItem>Undo</MenuItem>
      <MenuItem>Redo</MenuItem>
      <Divider/>
      <MenuItem>Cut</MenuItem>
      <MenuItem>Copy</MenuItem>
      <MenuItem>Paste</MenuItem>
      <Divider/>
      <SubMenu title={<span>Selection</span>}>
        <MenuItem>Select All</MenuItem>
        <MenuItem>Split Into Lines</MenuItem>
        <MenuItem>Single Selection</MenuItem>
        <Divider/>
        <SubMenu title={<span>Multiple Selections</span>}>
          <MenuItem>Add Cursor Up</MenuItem>
          <MenuItem>Add Cursor Down</MenuItem>
          <MenuItem>Move Active Cursor Up</MenuItem>
          <MenuItem>Move Active Cursor Down</MenuItem>
          <Divider/>
          <MenuItem>Add Next Selection Match</MenuItem>
          <MenuItem>Add Previous Selection Match</MenuItem>
          <Divider/>
          <MenuItem>Merge Selection Range</MenuItem>
        </SubMenu>
        <MenuItem>Select Word Right</MenuItem>
        <MenuItem>Select Word Left</MenuItem>
        <Divider/>
        <MenuItem>Select to Line End</MenuItem>
        <MenuItem>Select to Line Start</MenuItem>
        <Divider/>
        <MenuItem>Select to Document End</MenuItem>
        <MenuItem>Select to Document Start</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Line</span>}>
        <MenuItem>Indent</MenuItem>
        <MenuItem>Outdent</MenuItem>
        <MenuItem>Move Line Up</MenuItem>
        <MenuItem>Move Line Down</MenuItem>
        <Divider/>
        <MenuItem>Copy Lines Up</MenuItem>
        <MenuItem>Cope Lines Down</MenuItem>
        <Divider/>
        <MenuItem>Remove Line</MenuItem>
        <MenuItem>Remove to Line End</MenuItem>
        <MenuItem>Remove to Line Start</MenuItem>
        <Divider/>
        <MenuItem>Split Line</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Text</span>}>
        <MenuItem>Remove Word Right</MenuItem>
        <MenuItem>Remove Word Left</MenuItem>
        <Divider/>
        <MenuItem>Align</MenuItem>
        <MenuItem>Transpose Letters</MenuItem>
        <Divider/>
        <MenuItem>To Upper Case</MenuItem>
        <MenuItem>To Lower Case</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Comment</span>}>
        <MenuItem>Toggle Comment</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Code Folding</span>}>
        <MenuItem>Toggle Fold</MenuItem>
        <MenuItem>Unfold</MenuItem>
        <Divider/>
        <MenuItem>Fold Other</MenuItem>
        <MenuItem>Fold All</MenuItem>
        <MenuItem>Unfold All</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Code Formatting</span>}>
        <MenuItem>Apply Code Formatting</MenuItem>
        <MenuItem>Open Language & Formatting Preferences</MenuItem>
      </SubMenu>
    </SubMenu>
  );

export default EditMenu;