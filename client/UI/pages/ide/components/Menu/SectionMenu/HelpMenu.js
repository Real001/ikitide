import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const HelpMenu = (
    <SubMenu title={<span>Help</span>}>
      <MenuItem>Help</MenuItem>
      <MenuItem>Keymap Reference</MenuItem>
      <MenuItem>Demos and Screencasts</MenuItem>
      <MenuItem>Tip of the Day</MenuItem>
      <Divider/>
      <MenuItem>Submit Feedback</MenuItem>
      <Divider/>
      <MenuItem>About</MenuItem>
    </SubMenu>
  );

export default HelpMenu;