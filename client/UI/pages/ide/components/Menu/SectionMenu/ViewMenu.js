import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const ViewMenu = (
    <SubMenu title={<span>View</span>}>
      <MenuItem>Open Files</MenuItem>
      <MenuItem>Menu Bar</MenuItem>
      <MenuItem>Tab Buttons</MenuItem>
      <MenuItem>Gutter</MenuItem>
      <MenuItem>Status Bar</MenuItem>
      <MenuItem>Console</MenuItem>
      <Divider/>
      <SubMenu title={<span>Layout</span>}>
        <MenuItem>Single</MenuItem>
        <MenuItem>Vertical Split</MenuItem>
        <MenuItem>Horizontal Split</MenuItem>
        <MenuItem>Cross Split</MenuItem>
        <MenuItem>Split 1:2</MenuItem>
        <MenuItem>Split 2:1</MenuItem>
      </SubMenu>
      <Divider/>
      <SubMenu title={<span>Font Size</span>}>
        <MenuItem>Increase Font Size</MenuItem>
        <MenuItem>Decrease Font Size</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Syntax</span>}>
        <MenuItem>Auto-Select</MenuItem>
        <MenuItem>Plain Text</MenuItem>
        <Divider/>
        <MenuItem>C and C++</MenuItem>
        <MenuItem>C#</MenuItem>
        <MenuItem>CSS</MenuItem>
        <MenuItem>HTML</MenuItem>
        <MenuItem>Jade</MenuItem>
        <MenuItem>Java</MenuItem>
        <MenuItem>JavaScript</MenuItem>
        <MenuItem>JSON</MenuItem>
        <MenuItem>LESS</MenuItem>
        <MenuItem>Perl</MenuItem>
        <MenuItem>PHP</MenuItem>
        <MenuItem>Python</MenuItem>
        <MenuItem>Ruby</MenuItem>
        <MenuItem>Scala</MenuItem>
        <MenuItem>SCSS</MenuItem>
        <MenuItem>SQL</MenuItem>
        <MenuItem>Text</MenuItem>
        <MenuItem>XML</MenuItem>
        <Divider/>
      </SubMenu>
      <SubMenu title={<span>Themes</span>}>
        <MenuItem>Ambiance</MenuItem>
        <MenuItem>Chrome</MenuItem>
        <SubMenu title={<span>Clouds</span>}>
          <MenuItem>Clouds</MenuItem>
          <MenuItem>Clouds Midnight</MenuItem>
        </SubMenu>
        <MenuItem>Cobalt</MenuItem>
        <MenuItem>Crimson Editor</MenuItem>
        <MenuItem>Dawn</MenuItem>
        <MenuItem>Dreamweaver</MenuItem>
        <MenuItem>Eclipse</MenuItem>
        <MenuItem>GitHub</MenuItem>
        <MenuItem>Idle Fingers</MenuItem>
        <MenuItem>Kr Theme</MenuItem>
        <SubMenu title={<span>Merbivore</span>}>
          <MenuItem>Merbivore</MenuItem>
          <MenuItem>Merbivore Soft</MenuItem>
        </SubMenu>
        <MenuItem>Mono Industrial</MenuItem>
        <MenuItem>Monokai</MenuItem>
        <MenuItem>Pastel On Dark</MenuItem>
        <SubMenu title={<span>Solarized</span>}>
          <MenuItem>Solarized Dark</MenuItem>
          <MenuItem>Solarized Light</MenuItem>
        </SubMenu>
        <MenuItem>TextMate</MenuItem>
        <SubMenu title={<span>Tomorrow</span>}>
          <MenuItem>Tomorrow</MenuItem>
          <MenuItem>Tomorrow Night</MenuItem>
          <MenuItem>Tomorrow Night Blue</MenuItem>
          <MenuItem>Tomorrow Night Bright</MenuItem>
          <MenuItem>Tomorrow Night Eighties</MenuItem>
        </SubMenu>
        <MenuItem>Twilight</MenuItem>
        <MenuItem>Vibrant Ink</MenuItem>
        <MenuItem>Xcode</MenuItem>
      </SubMenu>
      <Divider/>
      <MenuItem>Wrap Lines</MenuItem>
      <MenuItem>Wrap To Print Margin</MenuItem>
    </SubMenu>
  );

export default ViewMenu;