import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const WindowMenu = (
    <SubMenu title={<span>Window</span>}>
      <MenuItem>New Terminal</MenuItem>
      <MenuItem>New Immediate Window</MenuItem>
      <MenuItem>Installer</MenuItem>
      <Divider/>
      <MenuItem>Outline</MenuItem>
      <MenuItem>Workspace</MenuItem>
      <MenuItem>Debugger</MenuItem>
      <MenuItem>Navigate</MenuItem>
      <MenuItem>Commands</MenuItem>
      <MenuItem>Changes</MenuItem>
      <Divider/>
      <SubMenu title={<span>Saved Layouts</span>}>
        <MenuItem>Save...</MenuItem>
        <MenuItem>Save And Close All...</MenuItem>
        <Divider/>
        <MenuItem>Show Saved Layouts in File Tree</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Tabs</span>}>
        <MenuItem>Close Pane</MenuItem>
        <MenuItem>Close All Tab In All Panes</MenuItem>
        <MenuItem>Close All But Current Tab</MenuItem>
        <Divider/>
        <MenuItem>Terminal</MenuItem>
        <MenuItem>Immediate</MenuItem>
        <Divider/>
        <MenuItem>Split Pane in Two Rows</MenuItem>
        <MenuItem>Split Pane in Two Columns</MenuItem>
      </SubMenu>
    </SubMenu>
  );

export default WindowMenu;