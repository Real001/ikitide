import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const GotoMenu = (
    <SubMenu title={<span>Goto</span>}>
      <MenuItem>Goto Anything...</MenuItem>
      <MenuItem>Goto Symbol...</MenuItem>
      <MenuItem>Goto Line...</MenuItem>
      <MenuItem>Goto Command...</MenuItem>
      <Divider/>
      <MenuItem>Next Error</MenuItem>
      <MenuItem>Previous Error</MenuItem>
      <Divider/>
      <MenuItem>Word Right</MenuItem>
      <MenuItem>Word Left</MenuItem>
      <Divider/>
      <MenuItem>Line End</MenuItem>
      <MenuItem>Line Start</MenuItem>
      <Divider/>
      <MenuItem>Jump to Definition</MenuItem>
      <MenuItem>Jump to Matching Brace</MenuItem>
      <Divider/>
      <MenuItem>Scroll to Selection</MenuItem>
    </SubMenu>
  );

export default GotoMenu;