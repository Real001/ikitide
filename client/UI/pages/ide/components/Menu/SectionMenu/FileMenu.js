import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const FileMenu = (
    <SubMenu title={<span>File</span>}>
      <MenuItem key="4-1">New File</MenuItem>
      <SubMenu title={<span>New From Template</span>}>
        <MenuItem>Text file</MenuItem>
        <MenuItem>JavaScript file</MenuItem>
        <MenuItem>Cpp file</MenuItem>
        <MenuItem>HTML file</MenuItem>
        <MenuItem>XML file</MenuItem>
        <MenuItem>CSS file</MenuItem>
        <MenuItem>SCSS file</MenuItem>
        <MenuItem>LESS file</MenuItem>
        <MenuItem>SVG file</MenuItem>
        <MenuItem>Python file</MenuItem>
        <MenuItem>PHP file</MenuItem>
        <MenuItem>Ruby file</MenuItem>
        <MenuItem>Prolog file</MenuItem>
        <MenuItem>Markdown</MenuItem>
        <MenuItem>Node.JS web server</MenuItem>
      </SubMenu>
      <MenuItem>Open...</MenuItem>
      <Divider/>
      <MenuItem>Save</MenuItem>
      <MenuItem>Save As...</MenuItem>
      <MenuItem>Save All</MenuItem>
      <Divider/>
      <MenuItem>Upload Local Files...</MenuItem>
      <MenuItem>Download Project</MenuItem>
      <Divider/>
      <MenuItem>Close File</MenuItem>
      <MenuItem>Close All Files</MenuItem>
    </SubMenu>
  );

export default FileMenu;