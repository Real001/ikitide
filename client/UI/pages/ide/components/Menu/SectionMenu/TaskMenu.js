import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const TaskMenu = (
  <SubMenu title={<span>Task</span>}>
    <MenuItem>Info</MenuItem>
    <MenuItem key="task_review">
      <button type="submit"  form='formIde'>
        Review
      </button>
    </MenuItem>
    <MenuItem>Refuse</MenuItem>
  </SubMenu>
);

export default TaskMenu;