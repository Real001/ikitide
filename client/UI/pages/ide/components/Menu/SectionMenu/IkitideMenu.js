import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const IKITIDEMenu = (
    <SubMenu title={<span>IKITIDE</span>} key="4">
      <MenuItem>About IKITIDE</MenuItem>
      <Divider/>
      <MenuItem>Preferences</MenuItem>
      <Divider/>
      <MenuItem>Go To You Dashboard</MenuItem>
      <MenuItem>Welcome Page</MenuItem>
      <Divider/>
      <MenuItem>Open Your Project Settings</MenuItem>
      <MenuItem>Open Your User Settings</MenuItem>
      <MenuItem>Open Your Keymap</MenuItem>
      <MenuItem>Open Your Init Script</MenuItem>
      <MenuItem>Open Your Stylesheet</MenuItem>
      <Divider/>
      <MenuItem>Restart IKITIDE</MenuItem>
      <MenuItem>Quit IKITIDE</MenuItem>
    </SubMenu>
  );

export default IKITIDEMenu;