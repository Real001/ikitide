import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const ToolsMenu = (
    <SubMenu title={<span>Tools</span>}>
      <MenuItem>Strip Trailing Space</MenuItem>
      <Divider/>
      <SubMenu title={<span>Preview</span>}>
        <MenuItem>Live Preview File</MenuItem>
        <MenuItem>Preview Running Application</MenuItem>
        <Divider/>
        <MenuItem>Configure Preview URL...</MenuItem>
        <MenuItem>Show Active Servers</MenuItem>
      </SubMenu>
      <MenuItem>Process List</MenuItem>
      <Divider/>
      <MenuItem>Show Autocomplete</MenuItem>
      <MenuItem>Rename Variable</MenuItem>
      <Divider/>
      <MenuItem>Toggle Macro Recording</MenuItem>
      <MenuItem>Play Macro</MenuItem>
      <Divider/>
      <SubMenu title={<span>Developer</span>}>
        <MenuItem>Start in Debug Mode</MenuItem>
      </SubMenu>
    </SubMenu>
  );

export default ToolsMenu;