import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const UserMenu = (
    <SubMenu title={<img src="/images/ava.png"></img>} className="user_menu">
      <MenuItem>Dashboard</MenuItem>
      <MenuItem>Account</MenuItem>
      <MenuItem>Log out</MenuItem>
    </SubMenu>
  );

export default UserMenu;