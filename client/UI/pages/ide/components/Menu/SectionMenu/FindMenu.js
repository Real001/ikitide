import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const FindMenu = (
    <SubMenu title={<span>Find</span>}>
      <MenuItem>Find...</MenuItem>
      <MenuItem>Find Next</MenuItem>
      <MenuItem>Find Previous</MenuItem>
      <Divider/>
      <MenuItem>Replace...</MenuItem>
      <MenuItem>Replace Next</MenuItem>
      <MenuItem>Replace Previous</MenuItem>
      <MenuItem>Replace All</MenuItem>
      <Divider/>
      <MenuItem>Find in File</MenuItem>
    </SubMenu>
  );

export default FindMenu;