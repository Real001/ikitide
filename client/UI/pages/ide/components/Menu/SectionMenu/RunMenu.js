import React, {Component} from 'react';
import Menu, {SubMenu, Item as MenuItem, Divider} from 'rc-menu';

const RunMenu = (
    <SubMenu title={<span>Run</span>}>
      <MenuItem>Run</MenuItem>
      <MenuItem>Run Last</MenuItem>
      <Divider/>
      <SubMenu title={<span>Run With</span>}>
        <MenuItem>C (make)</MenuItem>
        <MenuItem>C (simple)</MenuItem>
        <MenuItem>C++ (simple)</MenuItem>
        <MenuItem>CoffeeScript</MenuItem>
        <MenuItem>Java (single-file)</MenuItem>
        <MenuItem>Java (workspace)</MenuItem>
        <MenuItem>Node.JS</MenuItem>
        <MenuItem>PHP (built-in web server)</MenuItem>
        <MenuItem>PHP (cli)</MenuItem>
        <MenuItem>Python</MenuItem>
        <MenuItem>Python 2</MenuItem>
        <MenuItem>Ruby</MenuItem>
        <MenuItem>Ruby on Rails</MenuItem>
        <MenuItem>Shell command</MenuItem>
        <MenuItem>Shell script</MenuItem>
        <Divider/>
        <MenuItem>New Runner</MenuItem>
      </SubMenu>
      <SubMenu title={<span>Run History</span>}></SubMenu>
      <SubMenu title={<span>Run Configuration</span>}>
        <MenuItem>New Run Configuration</MenuItem>
        <MenuItem>Manage...</MenuItem>
      </SubMenu>
      <MenuItem>Show Debugger at Break</MenuItem>
      <Divider/>
      <MenuItem>Build</MenuItem>
      <MenuItem>Cancel Build</MenuItem>
      <SubMenu title={<span>Build System</span>}>
        <MenuItem>Automatic</MenuItem>
        <Divider/>
        <MenuItem>CoffeeScript</MenuItem>
        <MenuItem>SASS (SCSS)</MenuItem>
        <MenuItem>LESS</MenuItem>
        <MenuItem>Stylus</MenuItem>
        <MenuItem>Typescript</MenuItem>
        <Divider/>
        <MenuItem>New Build System</MenuItem>
      </SubMenu>
      <MenuItem>Show Build Result</MenuItem>
      <Divider/>
      <MenuItem>Automatically Build Supported Files</MenuItem>
      <MenuItem>Save All on Build</MenuItem>
    </SubMenu>
  );

export default RunMenu;