import React, {Component} from 'react';
import Menu, {Item as MenuItem} from 'rc-menu';
import PropTypes from 'prop-types';

import IKITIDEMenu from './SectionMenu/IkitideMenu';
import FileMenu from './SectionMenu/FileMenu';
import EditMenu from './SectionMenu/EditMenu';
import FindMenu from './SectionMenu/FindMenu';
import ViewMenu from './SectionMenu/ViewMenu';
import GotoMenu from './SectionMenu/GotoMenu';
import RunMenu from './SectionMenu/RunMenu';
import ToolsMenu from './SectionMenu/ToolsMenu';
import WindowMenu from './SectionMenu/WindowMenu';
import HelpMenu from './SectionMenu/HelpMenu';
import UserMenu from './SectionMenu/UserMenu';
import TaskMenu from './SectionMenu/TaskMenu';
import InfoTask from '../form/ModalInfoTask';

//Новый вариант, разобраться почему с ним не работает  меню
class SubMenu extends Component {
  state = {
    openTask: false,
    openKeys: null
  };

  onOpenChange = (openKeys) => {
    this.setState({
      openKeys,
    });
  };

  handleSelect = (info) => {
    // console.log(info);
    // console.log(`selected ${info.key}`);
    if (info.key === 'item_11-menu-item_0'){
      this.handleOpenTask();
    }
  };
  handleOpenTask = () => {
    this.setState({openTask: true});
  };
  handleCloseTask = () => {
    this.setState({openTask: false});
  };

  getMenu() {
    return (
      <Menu
        onClick={this.handleSelect}
        mode={this.props.mode}
        openAnimation={this.props.openAnimation}
        onOpenChange={this.onOpenChange}
        openKeys={this.state.openKeys}
      >
        <MenuItem><img src="/images/logo_ide.png"></img></MenuItem>
        {IKITIDEMenu}
        {FileMenu}
        {EditMenu}
        {FindMenu}
        {ViewMenu}
        {GotoMenu}
        {RunMenu}
        {ToolsMenu}
        {WindowMenu}
        {HelpMenu}
        {TaskMenu}
        <InfoTask openModal={this.state.openTask} handleClose={this.handleCloseTask} student={this.props.student}/>
        <MenuItem className="settings_menu"><img src="/images/settings.png"></img></MenuItem>
        {UserMenu}
      </Menu>
    )
  }

  render() {
    return (
      <div>
        {this.getMenu()}
      </div>
    )
  };
}
export default SubMenu