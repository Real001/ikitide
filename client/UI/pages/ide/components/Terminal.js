import React, {Component} from 'react';
import Terminal from 'terminal-in-react';
import NodeEvalPlugin from 'terminal-in-react-node-eval-plugin';
import ViPlugin from 'terminal-in-react-vi-plugin';
import pseudoFileSystemPlugin from 'terminal-in-react-pseudo-file-system-plugin';

const FileSystemPlugin = pseudoFileSystemPlugin();

class TerminalIde extends Component {
    render() {
        return (
              <div style={{
                height: "auto"
              }}>
              <Terminal
                color='green'
                backgroundColor='#f5f5f5'
                barColor='#202020'
                style={{ fontWeight: "bold", fontSize: "1em" }}
                startState="maximised"
                plugins={[
                  FileSystemPlugin,
                  {
                    class: ViPlugin,
                    config: {
                      filesystem: FileSystemPlugin.displayName
                    }
                  },
                  {
                    class: NodeEvalPlugin,
                    config: {
                      filesystem: FileSystemPlugin.displayName
                    }
                  }
                ]}
                commands={{
                  'open-google': () => window.open('https://www.google.com/', '_blank'),
                  popup: () => alert('Terminal in React')
                }}
                descriptions={{
                  'open-google': 'opens google.com',
                  showmsg: 'shows a message',
                  alert: 'alert', popup: 'alert'
                }}
                watchConsoleLogging={false}
              />
              </div>
        );
    }
}

export default TerminalIde;