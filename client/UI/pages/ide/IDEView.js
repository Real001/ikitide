import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import TopMenu from './components/Menu/TopMenu'
import Treebeard from './components/Treebeard.jsx';
import {renderCodeEditor} from '../../components/renderFields'
import TerminalIde from './components/Terminal'
import {data} from "./components/Treebeard/data";
import {Container, ContainerContent, Row, Col2, Col10, Col12} from "../../components/Layout";

import '../../style/App.css'

const IDEView = ({onReviewCode, handleClick, onSubmit, handleSubmit, student}) => {
  return (
    <div className="App">
        <TopMenu student={student}/>
        <Row>
          <Col2>
            <Treebeard data={data}/>
          </Col2>
          <Col10>
            <form onSubmit={handleSubmit} id='formIde'>
            <Field
              name="IDE"
              component={renderCodeEditor}
              mode="c_cpp"
              theme="monokai"
            />
            </form>
          </Col10>
        </Row>
        <Col12>
          <TerminalIde/>
        </Col12>
    </div>
  )
}

export default reduxForm()(IDEView);