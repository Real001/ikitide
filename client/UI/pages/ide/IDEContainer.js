import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {graphql,Query, Mutation} from 'react-apollo/index';
import {pipe} from 'ramda';
import IDEView from './IDEView';
import withUser from '../../components/withUser';
import {CodeReview} from "./queries"
import {GET_LESSON, GET_TASK} from '../desktop/queries';

class IDEContainer extends Component {

  static propTypes = {
    input: PropTypes.object,
    mode: PropTypes.string,
    theme: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    User: PropTypes.object
  };

  static defaultProps = {
    input: {},
    name: '',
    mode: '',
    theme: '',
    value: '',
  };

  onReviewCode = async (event) =>{
    const res = await this.props.addCodeReview({
      variables: {
        input: {
          user: this.props.User._id,
          lesson: this.props.data.startLessons.byGroup.academicSubject._id,
          code: event.IDE
        }
      }
    });
    alert('Ваш код отправлен на проверку');
    return res;
  };

  render() {
    return (
      <div>
      <IDEView
        onSubmit={this.onReviewCode}
        form="CreateIde"
        formName="CreateIde"
        student={this.props.User}
      />
        {/*<DevTools/>*/}
      </div>
    )
  }
}

export default pipe( connect(),
  graphql(CodeReview, {
    name: 'addCodeReview'}), graphql(GET_LESSON, {
    options: ownProps => ({
      variables: {group: (ownProps.User && ownProps.User.role) ? ownProps.User.role : ''}
    })}), withUser)(IDEContainer);