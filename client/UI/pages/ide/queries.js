import gql from 'graphql-tag';

export const CodeReview = gql`
    mutation CodeReview($input: CreateCompletedTaskInput!) {
        createCompletedTask(input: $input){
            _id
        }
    }
`;