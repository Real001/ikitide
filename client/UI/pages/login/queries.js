import gql from 'graphql-tag';

export const CREATE_MUTATION_USER = gql`
    mutation createUser($input: CreateUserInput!) {
        createUser(input: $input) {
            _id
        }
    }
`;

export const CREATE_TOKEN_BY_LOGIN = gql`
    mutation createTokenByLogin($login: String! $password: String!) {
        createTokenByLogin(input:{login:$login password:$password}){
            token
        }
    }
`;

export const GET_GROUP = gql`
    query GetGroup($offset: Int! $count: Int!)
    {
        groups{
            list(listOptions: {offset:$offset, count:$count}){
                items{
                    number
                }
            }
        }
    }
  `;