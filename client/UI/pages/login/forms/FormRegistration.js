import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {pipe} from 'ramda';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import MenuItem from 'material-ui/MenuItem'
import {
  SelectField,
  TextField,
} from 'redux-form-material-ui';
import PLogin from '../../../components/forms/PLogin';
import FormInputSimple from '../../../components/forms/FormInputSimple';
import SubmitButton from '../../../components/buttons/SubmitButton';
import GroupSelect from './components/GroupSelect'

const required = value => (value == null ? 'Поле обязательно для заполнения' : undefined);
class FormRegistration extends PureComponent{
  static propTypes = {
    handleSubmit: PropTypes.func,
    dispatch: PropTypes.func,
  };

  static defaultProps = {
    dispatch: () => {
    },
    handleSubmit: () => {
    },
    submitting: false
  };

  constructor(props) {
    super(props);
    this.renderFieldSimple = this.renderFieldSimple.bind(this);
  }

  renderFieldSimple({
                       input, type, title, required, name, placeholder
                     }) {
    return (
      <FormInputSimple
        input={input}
        title={title}
        required={required}
        type={type}
        placeholder={placeholder}
        name={name}
      />);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <PLogin title="Заполните следующие поля для того чтобы зарегистрироваться"/>
        <div>
        <Field
          name="lastName"
          hintText="Фамилия"
          required
          type="text"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="firstName"
          hintText="Имя"
          required
          type="text"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="middleName"
          hintText="Отчество"
          required
          type="text"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="role"
          required
          component={SelectField}
          hintText="Роль"
          validate={required}
        >
          <MenuItem value="Студент" primaryText="Студент"/>
          {/*<MenuItem value="Преподаватель" primaryText="Преподаватель"/>*/}
        </Field>
        </div>
        <div>
          <GroupSelect/>
        </div>
        <div>
        <Field
          name="login"
          hintText="Логин"
          required
          type="text"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="email"
          hintText="E-mail"
          required
          type="email"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="password"
          hintText="Пароль"
          required
          type="password"
          component={TextField}
          validate={required}
        />
        </div>
        <div>
        <Field
          name="passwordRepeat"
          hintText="Повторите пароль"
          required
          type="password"
          component={TextField}
          validate={required}
        />
        </div>
        <SubmitButton title="Регистрация"/>
      </form>
    )
  }
}


export default pipe(
  connect(),
  reduxForm({form: 'FormRegistration'})
)(FormRegistration);