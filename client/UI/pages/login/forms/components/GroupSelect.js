import React from 'react';
import { Query } from "react-apollo";
import {Field} from 'redux-form';
import MenuItem from 'material-ui/MenuItem'
import {SelectField,} from 'redux-form-material-ui';
import {GET_GROUP} from "../../queries"
const required = value => (value == null ? 'Поле обязательно для заполнения' : undefined);
const GroupSelect = () => (
  <Query query={GET_GROUP} variables={{offset:0, count: 10}}>
    {({loading, error, data}) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;
      return (
        <Field
          name="learnGroup"
          hintText="Группа"
          required
          type="text"
          component={SelectField}
          validate={required}
        >
          {data && data.groups && data.groups.list.items.map(item => (
            <MenuItem value={item.number} primaryText={item.number} />
          ))}
        </Field>
      )
    }}
  </Query>
);

export default GroupSelect;