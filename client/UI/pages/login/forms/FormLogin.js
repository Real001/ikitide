import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {pipe} from 'ramda';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import PLogin from '../../../components/forms/PLogin';
import FormInputSimple from '../../../components/forms/FormInputSimple';
import SubmitButton from '../../../components/buttons/SubmitButton';

class FormLogin extends PureComponent{
  static propTypes = {
    handleSubmit: PropTypes.func,
    dispatch: PropTypes.func,
  };

  static defaultProps = {
    dispatch: () => {
    },
    handleSubmit: () => {
    },
    submitting: false
  };

  constructor(props) {
    super(props);
    this.renderFieldSimple = this.renderFieldSimple.bind(this);
  }

  renderFieldSimple({
                      input, type, title, required, name, placeholder
                    }) {
    return (
      <FormInputSimple
        input={input}
        title={title}
        required={required}
        type={type}
        placeholder={placeholder}
        name={name}
      />);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <PLogin title="Введите имя пользователя и пароль"/>
        <Field
          name="login"
          placeholder="Логин"
          required
          type="text"
          component={this.renderFieldSimple}
        />
        <Field
          type="password"
          placeholder="Пароль"
          required
          name="password"
          component={this.renderFieldSimple}
        />
        <SubmitButton title="Войти"/>
      </form>
    )
  }
}


export default pipe(
  reduxForm({form: 'FormLogin'})
)(FormLogin);