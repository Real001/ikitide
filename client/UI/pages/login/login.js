import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {pipe} from 'ramda';
import {connect} from 'react-redux';
import {Tabs, Tab} from 'material-ui/Tabs';
import {graphql} from 'react-apollo';
import DocumentTitle from 'react-document-title';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ThemeDefault from '../theme-default';
import {CREATE_MUTATION_USER, CREATE_TOKEN_BY_LOGIN} from "./queries"
import FormLogin from './forms/FormLogin';
import FormRegistration from './forms/FormRegistration';

import '../../style/login.less';

const style = {
  width: 500,
  height: 600,
  margin: 'auto'
};


class Login extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    createUser: PropTypes.object
  };

  onCreateUser = async (event) => {
    const res = await this.props.createUser({
      variables: {
        input: {
          lastName: event.lastName,
          firstName: event.firstName,
          middleName: event.middleName,
          learnGroup: event.learnGroup && event.learnGroup.value,
          role: event.role && event.role.value,
          login: event.login,
          email: event.email,
          password: event.password,
        }
      }
    });
    this.onCreateToken(event);
    return res;
  };

  onCreateToken = async (event) => {
    const res = await this.props.createToken({
      variables: {login: event.login, password: event.password}
    });
    const token = res.data.createTokenByLogin.token;
    if (res.data.createTokenByLogin.token !== '') {
      localStorage.setItem('token', res.data.createTokenByLogin.token);
      this.props.history.push('/student/desktop');
    } else {
      alert(res.data.error);
    }

  };

  render() {
    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <DocumentTitle title="Вход и регистрация в IKITIDE">
          <body className="login">
          <div className="form form-signin">
            <div className="content" style={style}>
              <div className="card">
                <Tabs>
                  <Tab label="Авторизация">
                    <FormLogin onSubmit={this.onCreateToken}/>
                  </Tab>
                  <Tab label="Регистрация">
                    <FormRegistration onSubmit={this.onCreateUser}/>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
          </body>
        </DocumentTitle>
      </MuiThemeProvider>
    )
  }
}

export default pipe(
  graphql(CREATE_MUTATION_USER, {name: 'createUser'}),
  graphql(CREATE_TOKEN_BY_LOGIN, {name: 'createToken'}))(Login);