import gql from "graphql-tag";

export const GET_LESSON =  gql`
    query GetLesson($id: ObjectId!) {
        class{
            byId(_id: $id){
               name
                description
                material
            }
        }
    }
`;