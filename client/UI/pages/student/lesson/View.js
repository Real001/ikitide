import React, {Component} from 'react';
import MaterialLesson from '../../teacher/lesson/components/MaterialLesson';
import RaisedButton from 'material-ui/RaisedButton';

class ViewLesson extends Component {
  render() {
    return (
      <div>
        <MaterialLesson classId={this.props.match.params.id}/>
        <RaisedButton
          label={'Вернуться на рабочий стол'}
          primary={true}
          onClick={() => this.props.history.push('/student/desktop')}
        />

      </div>
    )
  }
}

export default ViewLesson;