import React, {Component} from 'react';
import {graphql} from 'react-apollo/index';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PageBase from '../../../components/base/PageBase';
import Tabs, { Tab } from 'material-ui/Tabs';
import StudentInfo from './components/StudentInfo';
import withUser from '../../../components/withUser';
// import {QueryById} from "./queries"

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class ProfileStudent extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0
    };
  }

  handleChange = (event, tabIndex) => {
    this.setState({ tabIndex });
  };

  render() {
    // const hasById = this.props.data.classes &&
    //   this.props.data.classes.byId
    //   && this.props.data.classes.byId;
    // if (this.props.data.loading) {
    //   return(<p>Loading....</p>)
    // } else {
      const { tabIndex } = this.state;
      return (
        <PageBase navigation={"Рабочий стол / Профиль  "}>
          <div>
            <Tabs>
              <Tab label="Мой профиль">
               <StudentInfo  student={this.props.User}/>
              </Tab>
              <Tab label="Мои успехи">
                Это пункт пока не работает, извините
              </Tab>
            </Tabs>
          </div>
        </PageBase>
      )
    }
  // }
}

export default pipe(
  connect(),
  // graphql(QueryById, {
  //   options: ownProps => ({
  //     fetchPolicy: 'network-only',
  //     variables: {id: ownProps.match.params.id}
  //   })
  // }),
  withRouter,
  withUser
)(ProfileStudent);