import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Card, {CardMedia, CardHeader, CardText} from 'material-ui/Card';
import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

const StudentInfo = ({student}) => {
    return (
      <div>
        <Card style={styles.card}>
          <CardHeader
            title={`${student.lastName} ${student.firstName} ${student.middleName}`}
            avatar="images/jsa-128.jpg"
          />
          <CardText>
            <div style={styles.title}>
              <InfoListDt title="Учебная группа"/>
              <InfoListDd title={student.learnGroup}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Email"/>
              <InfoListDd title={student.email}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Логин"/>
              <InfoListDd title={student.login}/>
            </div>
          </CardText>
        </Card>
      </div>
    )
};

export default StudentInfo;