import React from 'react';
import { Query } from "react-apollo";
import InfoTask from './components/InfoTask';
import {GET_TASK} from './queries'

const QueryInfoTask = ({startLesson}) => {
  return (
    <Query query={GET_TASK} variables={ {startLesson: startLesson }}>
      {({ loading, error, data }) => {
        return (<InfoTask
          loading={loading}
          error={error} data={data}
        />)
      }}
    </Query>
  )
};

export default QueryInfoTask;