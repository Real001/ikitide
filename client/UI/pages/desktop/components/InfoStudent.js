import React from 'react';
import DesktopStudent from '../../../components/desktop/DesktopStudent';

const InfoStudent = ({ loading, error, data, onViewLesson}) => {
  if (loading) return "Идет загрузка занятия подождите пожалуйста...";
  if (error) return `При загрузке занятия произошла ошибка! ${error.message}`;
  if (data.startLessons.byGroup !== undefined && data.startLessons.byGroup !== null) {
    return (
      <div>
        <h4>В данный момент у вас проводится занятие</h4>
        <DesktopStudent
          academicSubject={data.startLessons.byGroup.academicSubject.title}
          classes={data.startLessons.byGroup.classes.name}
          time={data.startLessons.byGroup.created}
          onViewLesson={onViewLesson}
        />
      </div>
    )
  }
  return "У вас нет занятий проводимых в данный момент"
};

export default InfoStudent;