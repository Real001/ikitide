import React, {PureComponent} from 'react';
import {withRouter, Redirect} from 'react-router-dom';
import {pipe} from 'ramda';
import {Query} from "react-apollo";
import PropTypes from 'prop-types';
import InfoStudent from './InfoStudent';
import QueryInfoTask from '../QueryInfoTask';
import {GET_LESSON} from '../queries'

class InfoUser extends PureComponent {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  onViewLesson = (id) => {
    this.props.history.push(`/student/lesson/${id}`);
  };

  render() {
    if (this.props.User.role === 'Студент') {
      let startLesson = null;
      return (
        <div>
          <h3>Вы успешно вошли в систему под ролью
            «<b>Студент</b>»</h3>
          <br/>
          <Query query={GET_LESSON} variables={{group: this.props.User.learnGroup}}>
            {({loading, error, data}) => {
              if (!loading) {
                startLesson = data && data.startLessons && data.startLessons.byGroup && data.startLessons.byGroup._id;

              }
              return (
                <div>
                  {(startLesson !== null || startLesson !== undefined) ?
                    <InfoStudent
                      loading={loading}
                      error={error} data={data}
                      onViewLesson={() => this.onViewLesson(data.startLessons.byGroup.classes._id)}
                    />
                  : 'У вас нет занятий проводимых в данный момент'}
                  <br/>
                  {(startLesson !== null || startLesson !== undefined) ? <QueryInfoTask startLesson={ startLesson}/> : ''}
                </div>)
            }}
          </Query>
        </div>
      )
    }
    return (
      <div>
        <h3>Вы успешно вошли в систему под ролью
          «<b>Преподаватель</b>»</h3>
      </div>
    )
  }
}

export default pipe(withRouter)(InfoUser);