import React from 'react';
import DesktopStudent from '../../../components/desktop/DesktopStudent';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

const InfoTask = ({ loading, error, data}) => {
  if (loading) return "Идет загрузка занятия подождите пожалуйста...";
  if (error) return `При загрузке занятия произошла ошибка! ${error.message}`;
  if (data.executableTasks.byStartLesson !== undefined && data.executableTasks.byStartLesson !== null) {
    const executableTask = data.executableTasks.byStartLesson;
    return (
      <div>
       <Card>
         <CardHeader
           title="У вас есть новая задача!"
           subtitle={executableTask.task.name}
           actAsExpander={true}
           showExpandableButton={true}
         />
         <CardText expandable={true}>
           <div>
             {executableTask.task.description}
           </div>
           <CardActions>
             <FlatButton label="Приступить к выполнению" />
             <FlatButton label="Не выполнять" />
           </CardActions>
         </CardText>
       </Card>
      </div>
    )
  }
  return "У вас нет задач для выполнения"
};

export default InfoTask;