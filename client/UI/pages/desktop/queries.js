import gql from "graphql-tag";

export const GET_LESSON = gql`
    query GetLesson($group: String!) {
        startLessons{
            byGroup(group: $group){
                _id
                academicSubject {
                    title
                    _id
                }
                classes {
                    _id
                    name
                }
                created
            }
        }
    }
`;

export const GET_TASK = gql`
    query GetTask($startLesson: ObjectId!) {
        executableTasks{
            byStartLesson(startLesson: $startLesson){
                task{
                    name
                    description
                    inputData
                    outputData
                }
            }
        }
    }
`;