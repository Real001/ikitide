import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Card from 'material-ui/Card/Card';
import CardTitle from 'material-ui/Card/CardTitle'
import InfoUser from './components/InfoUser';
import withUser from '../../components/withUser';
import '../../style/components/main/Body.less';

const style = {
  width: 960,
  height: 600,
  margin: 'auto'
};

class Desktop extends PureComponent {
  static propTypes = {
    User: PropTypes.object
  };

  render() {
    return (

        <div className="center">
          <div className="block colcontent">
            <Card style={style}>
              <CardTitle>
                <h2>Добро пожаловать!</h2>
              </CardTitle>
              <CardTitle>
                <div className="content">
                  <InfoUser
                    User={this.props.User}
                  />
                </div>
              </CardTitle>
            </Card>
          </div>
        </div>
    );
  }
}

export default (withUser)(Desktop);