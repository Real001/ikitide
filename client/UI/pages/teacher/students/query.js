import gql from 'graphql-tag';

export const StudentsQuery = gql`
    query GetStudents($count: Int! $offset: Int!){
        users {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    lastName
                    firstName
                    middleName
                    learnGroup
                }
            }
        }
    }
`;


export const QueryById = gql`
    query ById($id: ObjectId!) {
        users{
            byId(_id: $id) {
                lastName
                firstName
                middleName
                email
                login
                learnGroup
            }
        }
    }
`;