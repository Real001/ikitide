import React, {Component} from 'react';
import {graphql} from 'react-apollo/index';
// import { withStyles } from 'material-ui/styles';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Tabs, { Tab } from 'material-ui/Tabs';
import PageBase from '../../../components/base/PageBase';
import StudentCommonInfo from './components/StudentCommonInfo';
import {QueryById} from "./query"
import StudentStatics from './components/StudentStatics'

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class StudentInfo extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  render() {
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      const hasById = this.props.data.users &&
        this.props.data.users.byId
        && this.props.data.users.byId;
      return (
        <PageBase title="Информация о студенте"
                  navigation={"Рабочий стол / Студенты / " + hasById.lastName + ' ' + hasById.firstName +
                  ' ' + hasById.middleName }>
          <div>
            <Tabs>
              <Tab label="Информация">
                <StudentCommonInfo common={this.props.data.users.byId}/>
              </Tab>
              <Tab label="Статистика">
                <StudentStatics/>
              </Tab>
            </Tabs>
          </div>
        </PageBase>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(QueryById, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {id: ownProps.match.params.id}
    })
  }),
  withRouter
)(StudentInfo);