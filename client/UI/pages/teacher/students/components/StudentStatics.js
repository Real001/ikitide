import React, {Component} from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {withRouter, Link} from 'react-router-dom';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {grey200, grey500} from 'material-ui/styles/colors';
import ContentPaste from 'material-ui/svg-icons/content/content-paste';
import {statics} from "./dataStitics"

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  editButton: {
    fill: grey500
  },
  viewButton: {
    fill: grey500
  },
  columns: {
    id: {
      width: '10%'
    },
    name: {
      width: '30%'
    },
    price: {
      width: '5%'
    },
    category: {
      width: '5%'
    },
    academicSubject: {
      width: '10%'
    },
    edit: {
      width: '10%'
    },
    view: {
      width: '10%'
    },
  },
  progress: {
    margin: 10,
  },
};

const StudentStatics = () => {
  return(
    <div>
      <Table>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn>Задача</TableHeaderColumn>
            <TableHeaderColumn>Лекция</TableHeaderColumn>
            <TableHeaderColumn>Предмет</TableHeaderColumn>
            <TableHeaderColumn>Результат</TableHeaderColumn>
            <TableHeaderColumn>Просмотр кода</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
          {statics.map(item => (
              <TableRow key={item._id}>
                <TableRowColumn>{item.task}</TableRowColumn>
                <TableRowColumn>{item.lecture}</TableRowColumn>
                <TableRowColumn>{item.academicSubject}</TableRowColumn>
                <TableRowColumn>{item.rating}</TableRowColumn>
                <TableRowColumn>
                  <Link className="button" to={"/teacher/classes/view/" + item._id}>
                    <FloatingActionButton zDepth={0}
                                          mini={true}
                                          backgroundColor={grey200}
                                          iconStyle={styles.viewButton}>
                      <ContentPaste  />
                    </FloatingActionButton>
                  </Link>
                </TableRowColumn>
              </TableRow>
            )
          )}
        </TableBody>
      </Table>
    </div>
  )
};

export default StudentStatics;