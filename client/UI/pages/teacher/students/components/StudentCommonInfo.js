import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Card, {CardMedia, CardText} from 'material-ui/Card';
import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class StudentCommonInfo extends Component {
  static propTypes = {
    common: PropTypes.object
  };

  static defaultProps = {
    common: {}
  };

  render() {
    const {common} = this.props;
    return (
      <div>
        <Card style={styles.card}>
          <CardText>
            <div style={styles.title}>
              <InfoListDt title="Фамилия"/>
              <InfoListDd title={common.lastName ? common.lastName  : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Имя"/>
              <InfoListDd title={common.firstName ? common.firstName : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Отчество"/>
              <InfoListDd title={common.middleName ? common.middleName: ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Учебная группа"/>
              <InfoListDd title={common.learnGroup ? common.learnGroup : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="E-mail"/>
              <InfoListDd title={common.email ? common.email : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Логин"/>
              <InfoListDd title={common.login ? common.login : ''}/>
            </div>
          </CardText>
        </Card>
      </div>
    )
  }
}

export default StudentCommonInfo;