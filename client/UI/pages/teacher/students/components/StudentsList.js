import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import {grey200, grey500} from 'material-ui/styles/colors';
import ContentPaste from 'material-ui/svg-icons/content/content-paste';
import {StudentsQuery} from "../query"

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  editButton: {
    fill: grey500
  },
  viewButton: {
    fill: grey500
  },
  columns: {
    id: {
      width: '10%'
    },
    name: {
      width: '30%'
    },
    price: {
      width: '5%'
    },
    category: {
      width: '5%'
    },
    academicSubject: {
      width: '10%'
    },
    edit: {
      width: '10%'
    },
    view: {
      width: '10%'
    }
  }
};


class StudentsList extends Component {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func
    }),
    onTotalCallBack: PropTypes.func,
    data: PropTypes.shape({
      tasks: PropTypes.shape()
    })
  };

  static defaultProps = {
    history: {},
    onTotalCallBack: () => {
    },
    data: {}
  };

  render() {
    const hasList = this.props.data.tasks &&
      this.props.data.tasks.list
      && this.props.data.tasks.list.items;
    if (hasList) {
      this.props.onTotalCallBack(this.props.data.tasks.list.meta.total);
    } else {
      this.props.onTotalCallBack(0);
    }
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return(
        <div>
          {this.props.data.users.list.items.map(item => (
              <TableRow key={item._id}>
                <TableRowColumn style={styles.columns.id}><div>1</div></TableRowColumn>
                <TableRowColumn style={styles.columns.name}>
                  {item.lastName + ' ' + item.firstName + ' ' + item.middleName}
                  </TableRowColumn>
                <TableRowColumn style={styles.columns.price}>{item.learnGroup}</TableRowColumn>
                <TableRowColumn style={styles.columns.view}>
                  <Link className="button" to={"/teacher/student/view/" + item._id}>
                    <FloatingActionButton zDepth={0}
                                          mini={true}
                                          backgroundColor={grey200}
                                          iconStyle={styles.viewButton}>
                      <ContentPaste  />
                    </FloatingActionButton>
                  </Link>
                </TableRowColumn>
              </TableRow>
            )
          )}
        </div>
      )
    }
  }
}

export default pipe(
  withRouter,
  graphql(StudentsQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        },
        findOptions: {
          role: 'Студент'
        }
      }
    })
  })
)(StudentsList);