import React from 'react';
import {Link} from 'react-router-dom';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentPaste from 'material-ui/svg-icons/content/content-paste';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../../../components/base/PageBase';
import StudentsList from './components/StudentsList';
import Data from './data';

const StudentsPage = () => {

  const styles = {
    floatingActionButton: {
      margin: 0,
      top: 'auto',
      right: 20,
      bottom: 20,
      left: 'auto',
      position: 'fixed',
    },
    viewButton: {
      fill: grey500
    },
    columns: {
      id: {
        width: '10%'
      },
      fio: {
        width: '50%'
      },
      group: {
        width: '30%'
      },
      view: {
        width: '10%'
      }
    }
  };

  return (
    <PageBase title={"Список студентов"}
              navigation="Рабочий стол / Студенты">

      <div>
        {/*<Link to="/teacher/classes/add" >*/}
          {/*<FloatingActionButton style={styles.floatingActionButton} backgroundColor={pink500}>*/}
            {/*<ContentAdd />*/}
          {/*</FloatingActionButton>*/}
        {/*</Link>*/}

        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
              <TableHeaderColumn style={styles.columns.fio}>ФИО</TableHeaderColumn>
              <TableHeaderColumn style={styles.columns.group}>Группа</TableHeaderColumn>
              <TableHeaderColumn style={styles.columns.view}>Просмотр</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            <StudentsList
              offset="0"
              count="10"
            />
          </TableBody>
        </Table>
      </div>
    </PageBase>
  );
};

export default StudentsPage;