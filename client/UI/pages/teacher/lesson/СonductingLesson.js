import React, {Component} from 'react';
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {Mutation, graphql} from 'react-apollo';
import {connect} from 'react-redux';
import {pipe} from 'ramda';

import MaterialLesson from './components/MaterialLesson'
import SelectTask from './components/SelectTask';
import SettingTask from './components/SettingTask';
import StartTask from './components/StartTask';
import {START_TASK_MUTATION, DELETE_START_TASK_MUTATION, DELETE_START_LESSON_MUTATION} from "./query";
import {setInitialDataStartTask} from "../../../redux/actions/lesson"

class СonductingLesson extends Component {
  state = {
    finished: false,
    stepIndex: 0,
  };

  handleNext = async () => {
    const {stepIndex} = this.state;
    if (stepIndex === 3) {
      const res = await this.props.deleteStartLessonkMutation({
        variables: {
          id: this.props.initialTask.startLesson
        }
      });
    }
    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex >= 7,
    });
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  onStartTask = async () => {
    if (!this.props.initialTask.startTask) {
      const res = await this.props.startTaskMutation({
        variables: {
          input: {
            startLesson: this.props.initialTask.startLesson,
            task: this.props.initialTask.taskId
          }
        }
      });
      this.props.dispatch(setInitialDataStartTask(res.data.createExecutableTask._id));
    } else {
      const res = await this.props.deleteStartTaskMutation({
        variables: {
          id: this.props.initialTask.startTask
        }
      });
      this.props.dispatch(setInitialDataStartTask(null));
    }
  };


  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <MaterialLesson classId={this.props.initialTask.classesId}/>
        );
      case 1:
        return (
          <SelectTask themeClass={this.props.initialTask.classesId}/>
        );
      case 2:
        return (<SettingTask/>);
      case 3:
        return (
          <StartTask onStartTask={() => {
            this.onStartTask()
          }}/>
        );
      default:
        return 'Занятие подошло к концу';
    }
  }

  render() {
    const {finished, stepIndex} = this.state;
    const contentStyle = {margin: '0 10px'};
    return (
      <div style={{width: '100%', maxWidth: 1200, margin: 'auto'}}>
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Проведение занятия</StepLabel>
          </Step>
          <Step>
            <StepLabel>Выбор задачи</StepLabel>
          </Step>
          <Step>
            <StepLabel>Настройка задачи</StepLabel>
          </Step>
          <Step>
            <StepLabel>Выполнение задачи</StepLabel>
          </Step>
        </Stepper>
        <div style={contentStyle}>
          {finished ? (
            <p>
              <a
                href="#"
                onClick={(event) => {
                  event.preventDefault();
                  this.setState({stepIndex: 0, finished: false});
                }}
              >
                Click here
              </a> to reset the example.
            </p>
          ) : (
            <div>
              <div>{this.getStepContent(stepIndex)}</div>
              <div style={{marginTop: 12}}>
                <FlatButton
                  label="Назад"
                  disabled={stepIndex === 0}
                  onClick={this.handlePrev}
                  style={{marginRight: 12}}
                />
                <RaisedButton
                  label={stepIndex === 3 ? 'Закончить проведение занятия' : 'Далее'}
                  primary={true}
                  onClick={this.handleNext}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    initialTask: state.pages.lesson,
  }
};


export default pipe(connect(mapStateToProps),
  graphql(START_TASK_MUTATION, {
    name: 'startTaskMutation'
  }), graphql(DELETE_START_TASK_MUTATION, {
    name: 'deleteStartTaskMutation'
  }), graphql(DELETE_START_LESSON_MUTATION, {
    name: 'deleteStartLessonkMutation'
  }))(СonductingLesson);