import React, {Component} from 'react';
import {Redirect } from 'react-router'
import {Step, Stepper, StepLabel} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {Mutation, graphql} from 'react-apollo';
import {connect} from 'react-redux';
import {pipe} from 'ramda';

import LessonCommonInfo from './components/LessonCommonInfo';
import SelectGroup from './components/SelectGroup';
import SelectAcademicSubject from './components/SelectAcademicSubject';
import SelectClasses from './components/SelectClasses';
import {START_LESSON_MUTATION} from "./query";
import {setInitialDataStartLesson} from "../../../redux/actions/lesson"

const user = {
  firstName: "Александр",
  middleName: "Иванович"
};


class LessonPage extends Component {
  state = {
    finished: false,
    stepIndex: 0,
  };

  handleNext = async () => {
    const {stepIndex} = this.state;
    if (stepIndex === 3) {
      const res = await this.props.startLessonMutation({
        variables: {
          input: {
            groups: this.props.initialTask.groups,
            academicSubject: this.props.initialTask.academicSubjectId,
            classes: this.props.initialTask.classesId,
          }
        }
      });
      this.props.dispatch(setInitialDataStartLesson(res.data.createStartLesson._id));
    }
    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex >= 3,
    });
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <LessonCommonInfo user={user}/>
        );
      case 1:
        return (
          <SelectGroup/>
        );
      case 2:
        return (
          <SelectAcademicSubject/>
        );
      case 3:
        return (
          <SelectClasses/>
        );
      default:
        return 'You\'re a long way from home sonny jim!';
    }
  }

  render() {
    const {finished, stepIndex} = this.state;
    const contentStyle = {margin: '0 16px'};
    return (
      <Card style={{width: '100%', maxWidth: 1200, margin: 'auto'}}>
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Настройка проведения занятия</StepLabel>
          </Step>
          <Step>
            <StepLabel>Выбор группы</StepLabel>
          </Step>
          <Step>
            <StepLabel>Выбор предмета</StepLabel>
          </Step>
          <Step>
            <StepLabel>Выбор темы занятия</StepLabel>
          </Step>
        </Stepper>
        <div style={contentStyle}>
          {finished ? (
           <Redirect to="/teacher/lesson"/>
          ) : (
            <div>
              <div>{this.getStepContent(stepIndex)}</div>
              <div style={{marginTop: 12}}>
                <FlatButton
                  label="Назад"
                  disabled={stepIndex === 0}
                  onClick={this.handlePrev}
                  style={{marginRight: 12}}
                />
                <RaisedButton
                  label={stepIndex === 3 ? 'Начать занятие' : 'Далее'}
                  primary={true}
                  onClick={this.handleNext}
                />
              </div>
            </div>
          )}
        </div>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    initialTask: state.pages.lesson,
  }
};

export default pipe(connect(mapStateToProps),
  graphql(START_LESSON_MUTATION, {
    name: 'startLessonMutation'
  }))(LessonPage)
