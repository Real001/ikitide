import gql from 'graphql-tag';

export const AcademicSubjectsQuery = gql`
    query GetAcademicSubjects($count: Int! $offset: Int!){
        academicSubjects {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    name
                    title
                }
            }
        }
    }
`;

export const ClassesQuery = gql`
    query GetClasses($count: Int! $offset: Int!){
        classes {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    name
                }
            }
        }
    }
`;

export const QueryById = gql`
    query ById($id: ObjectId!) {
        classes{
            byId(_id: $id) {
                name
                description
                semester
                course
                material
                #academicSubject
            }
        }
    }
`;

export const TasksQuery = gql`
    query GetTasks($count: Int! $offset: Int! $themeClass: ObjectId){
        tasks {
            list(listOptions:{offset: $offset count:$count} findOptions: {themeClass: $themeClass}){
                meta{
                    total
                }
                items{
                    _id
                    name
                    #                    academicSubject
                    themeClass {
                        name
                    }
                }
            }
        }
    }
`;


export const TaskById = gql`
    query ById($id: ObjectId!) {
        tasks{
            byId(_id: $id) {
                name
                description
                themeClass {
                    name
                }
                inputData
                outputData
                academicSubject{
                    title
                }
                testData{
                    inputData
                    outputData
                }
                time
            }
        }
    }
`;

export const StudentsQuery = gql`
    query GetStudents($count: Int! $offset: Int!){
        users {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    lastName
                    firstName
                    middleName
                    learnGroup
                }
            }
        }
    }
`;

export const GroupsQuery = gql`
  query GetGroups($count: Int! $offset: Int!) {
      groups{
          list(listOptions: {offset:$offset, count:$count}){
              meta{
                  total
              }
              items{
                  _id
                  number
                  course
                  power
              }
          }
      }
  }
`;

export const START_LESSON_MUTATION = gql`
    mutation createStartLesson($input: CreateStartLessonInput!) {
        createStartLesson(input: $input){
            _id
        }
    }
`;

export const DELETE_START_LESSON_MUTATION = gql`
    mutation deleteStartLesson($id: ObjectId!) {
        deleteStartLesson(id: $id){
            _id
        }
    }
`;

export const START_TASK_MUTATION = gql`
    mutation createExecutableTask($input: CreateExecutableTaskInput!) {
        createExecutableTask(input: $input){
            _id
        }
    }
`;

export const DELETE_START_TASK_MUTATION = gql`
    mutation deleteExecutableTask($id: ObjectId!) {
        deleteExecutableTask(id: $id){
            _id
        }
    }
`;
