import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import Divider from 'material-ui/Divider';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';

import {GroupsQuery} from "../query"
import {setInitialDataGroups} from "../../../../redux/actions/lesson"

const styles = {
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
};

class SelectGroup extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };


  state = {
    checked: false,
  };

  updateCheck = (e) => {
    this.props.dispatch(setInitialDataGroups(e.target.value));
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  };

  filter = (groups) => {

    const variables = {};
    variables.bacOne = groups.filter(group => (group.power === 'Бакалавриат' && group.course === 1));
    variables.bacTwo = groups.filter(group => (group.power === 'Бакалавриат' && group.course === 2));
    variables.bacThree = groups.filter(group => (group.power === 'Бакалавриат' && group.course === 3));
    variables.bacFour = groups.filter(group => (group.power === 'Бакалавриат' && group.course === 4));
    variables.magOne = groups.filter(group => (group.power === 'Магистратура' && group.course === 1));
    variables.magTwo = groups.filter(group => (group.power === 'Магистратура' && group.course === 2));
    return variables;
  };

  render() {
    if (this.props.data.loading) {
      return (<p>Loading....</p>)
    } else if (this.props.data.error) {
      return (<p>{this.props.data.error}</p>);
    }
    const groups = this.props.data.groups &&
      this.props.data.groups.list
      && this.props.data.groups.list;
    if (groups) {
      return (
        <CardText>
          <h3>Выберите группу или группы у которых хотите провести занятие</h3>
          <h4>Бакалавриат</h4>
          <h5>1 курс</h5>
          <List>
            {this.filter(groups.items).bacOne.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number} />}/>
            ))}
          </List>
          <Divider/>
          <h5>2 курс</h5>
          <List>
            {this.filter(groups.items).bacTwo.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number}/>}/>
            ))}
          </List>
          <Divider/>
          <h5>3 курс</h5>
          <List>
            {this.filter(groups.items).bacThree.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number}/>}/>
            ))}
          </List>
          <Divider/>
          <h5>4 курс</h5>
          <List>
            {this.filter(groups.items).bacFour.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number}/>}/>
            ))}
          </List>
          <Divider/>
          <h4>Магистратура</h4>
          <h5>1 курс</h5>
          <List>
            {this.filter(groups.items).magOne.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number}/>}/>
            ))}
          </List>
          <Divider/>
          <h5>2 курс</h5>
          <List>
            {this.filter(groups.items).magTwo.map(item => (
              <ListItem key={item._id} primaryText={item.number} rightIcon={<Checkbox
                style={styles.checkbox} onCheck={this.updateCheck} value={item.number}/>}/>
            ))}
          </List>
          <Divider/>
        </CardText>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(GroupsQuery, {
    options: ownProps => ({
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        }
      }
    })
  }),
  withRouter
)(SelectGroup);