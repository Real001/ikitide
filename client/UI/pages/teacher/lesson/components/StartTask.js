import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql, Query} from 'react-apollo/index';
import RaisedButton from 'material-ui/RaisedButton';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';

import StudentsList from './StudentsList'

class StartTask extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      start: false
    }
  }

  startTask = () =>{
    this.setState(() => ({start: !this.state.start}));
    this.props.onStartTask();
  };

  render() {
    const isStart = this.state.start;
    const Students = isStart ? (
      <StudentsList/>
    ): (
      <div/>
    );
      return (
        <CardText>
          <h3>Выполнение задачи студентами</h3>
          <div>
            <RaisedButton
              label={this.state.start === true ? 'Закончить выполнеение задачи' : 'Начать выполнение задачи'}
              primary={true}
              onClick={this.startTask}
            />
            {Students}
          </div>
        </CardText>
      )
    }
}

export default pipe(
  connect(),
  withRouter
)(StartTask);