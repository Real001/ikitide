import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import {ClassesQuery} from "../query"
import {setInitialDataClasses} from "../../../../redux/actions/lesson"


class SelectClasses extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  onSelectClasses = (e) => {
    this.props.dispatch(setInitialDataClasses(e.target.value));
  };

  render() {
    const hasByList = this.props.data.classes &&
      this.props.data.classes.list
      && this.props.data.classes.list;
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return (
        <CardText>
          <h3>Выберите тему предстоящего занятия</h3>
          <div>
            <RadioButtonGroup name="classes" onChange={this.onSelectClasses}>
              {this.props.data.classes.list.items.map(item => (
                <RadioButton value={item._id} label={item.name}/>
               ))
              }
            </RadioButtonGroup>
          </div>
        </CardText>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(ClassesQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        }
      }
    })
  }),
  withRouter
)(SelectClasses);