import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql, Query} from 'react-apollo/index';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {grey200, grey500} from 'material-ui/styles/colors';
import ContentPaste from 'material-ui/svg-icons/content/content-paste';

import {StudentsQuery} from "../query"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  viewButton: {
    fill: grey500
  },
};

const StudentsList = (props) => {
  if (props.data.loading) {
    return(<p>Loading....</p>)
  } else {
    return (
      <div>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn>ФИО</TableHeaderColumn>
              <TableHeaderColumn>Группа</TableHeaderColumn>
              <TableHeaderColumn>Попытка</TableHeaderColumn>
              <TableHeaderColumn>Выполнено</TableHeaderColumn>
              <TableHeaderColumn>Просмотр статистики</TableHeaderColumn>
          </TableRow>
          </TableHeader>
          <TableBody>
            {props.data.users.list.items.map(item => (
                <TableRow key={item._id}>
                  <TableRowColumn>
                    {item.lastName + ' ' + item.firstName + ' ' + item.middleName}
                  </TableRowColumn>
                  <TableRowColumn>{item.learnGroup}</TableRowColumn>
                  <TableRowColumn>1</TableRowColumn>
                  <TableRowColumn>В процессе</TableRowColumn>
                  <TableRowColumn>
                    <Link className="button" to={"/teacher/lesson/student/statistics/" + item._id}>
                      <FloatingActionButton zDepth={0}
                                            mini={true}
                                            backgroundColor={grey200}
                                            iconStyle={styles.viewButton}>
                        <ContentPaste/>
                      </FloatingActionButton>
                    </Link>
                  </TableRowColumn>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
};

StudentsList.propTypes = {
  history: PropTypes.object,
  data: PropTypes.object,
  dispatch: PropTypes.func,
};

StudentsList.defaultProps = {
  history: {},
  dispatch: () => {
  }
};

export default pipe(
  connect(),
  graphql(StudentsQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        },
        findOptions: {
          role: 'Студент'
        }
      }
    })
  }),
  withRouter
)(StudentsList)

