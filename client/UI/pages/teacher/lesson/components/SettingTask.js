import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql, Query} from 'react-apollo/index';
import {List, ListItem} from 'material-ui/List';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';

import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"
import {TaskById} from "../query"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

const SettingTask = (props) => (

    <Query
      query={TaskById}
      variables={{id: props.taskId}}
    >
      {({loading, error, data}) => {
        if (loading) return null;
        if (error) return `Error!: ${error}`;
        const task = data.tasks.byId;
        return (
          <CardText>
            <h3>Произведите настройку задачи, для ее выполнения студентами</h3>
            <div>
              <div style={styles.title}>
                <InfoListDt title="Название"/>
                <InfoListDd title={task.name ? task.name : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Учебный предмет"/>
                <InfoListDd title={task.academicSubject ? task.academicSubject.title : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Описание"/>
                <InfoListDd title={task.description ? task.description : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Темя занятия"/>
                <InfoListDd title={task.themeClass ? task.themeClass.name : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Время на выполнение"/>
                <InfoListDd title={task.time ? `${task.time} минут` : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Входные данные"/>
                <InfoListDd title={task.inputData ? task.inputData : ''}/>
              </div>
              <div style={styles.title}>
                <InfoListDt title="Выходные данные"/>
                <InfoListDd title={task.outputData ? task.outputData : ''}/>
              </div>
              <div>
                <h4>Тестовые данные для проверки</h4>
                {task.testData && task.testData.map((item, index) => (
                  <div>
                    <p>{`Тестовые данные №${index + 1}`}</p>
                    <div style={styles.title}>
                      <InfoListDt title="Входные данные"/>
                      <InfoListDd title={item.inputData}/>
                    </div>
                    <div style={styles.title}>
                      <InfoListDt title="Выходные данные"/>
                      <InfoListDd title={item.outputData}/>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </CardText>
        );
      }}
    </Query>
);

SettingTask.propTypes = {
  history: PropTypes.object,
  data: PropTypes.object,
  dispatch: PropTypes.func,
  taskId: PropTypes.object.isRequired
};

SettingTask.defaultProps = {
  history: {},
  dispatch: () => {
  }
};


const mapStateToProps = state => ({
  taskId: state.pages.lesson.taskId
});

export default pipe(
  connect(mapStateToProps),
  withRouter
)(SettingTask)

