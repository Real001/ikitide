import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';


class LessonCommonInfo extends Component {
  static propTypes = {
    user: PropTypes.object
  };

  render() {
    return (
      <div>
        <CardText>
          <h2>Здравствуйте уважаемый, {this.props.user.firstName} {this.props.user.middleName}!</h2>
          <p>Для того чтобы начать проведение занятия, нажмите на кнопку ниже и следуйте дальнейшим указаниям.</p>
        </CardText>
      </div>
    )
  }
}

export default LessonCommonInfo;