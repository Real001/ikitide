import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import {AcademicSubjectsQuery} from "../query"
import {setInitialDataAcademicSubject} from "../../../../redux/actions/lesson"



class SelectAcademicSubject extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  onSelectAcademicSubject = (e) => {
    this.props.dispatch(setInitialDataAcademicSubject(e.target.value));
  };

  render() {
    const hasByList = this.props.data.academicSubjects &&
      this.props.data.academicSubjects.list
      && this.props.data.academicSubjects.list;
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return (
        <CardText>
          <h3>Выберите предмет который планируете провести</h3>
          <div>
            <RadioButtonGroup name="academicSubjects" onChange={this.onSelectAcademicSubject}>
               {this.props.data.academicSubjects.list.items.map(item => (
                   <RadioButton value={item._id} label={item.title}/>
                 ))
               }
            </RadioButtonGroup>
          </div>
        </CardText>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(AcademicSubjectsQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        }
      }
    })
  }),
  withRouter
)(SelectAcademicSubject);