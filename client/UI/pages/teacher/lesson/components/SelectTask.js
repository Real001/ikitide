import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {List, ListItem} from 'material-ui/List';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import {TasksQuery} from "../query"
import {setInitialDataTask} from "../../../../redux/actions/lesson"


class SelectTask extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  constructor(props) {
    super(props);
    this.onRedirect = this.onRedirect.bind(this);
  }

  onRedirect = (e) => {
    this.props.dispatch(setInitialDataTask(e.target.value));
    // this.props.history.push(`/teacher/lesson/task/${id}`)
  };

  render() {
    const hasByList = this.props.data.tasks &&
      this.props.data.tasks.list
      && this.props.data.tasks.list;
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return (
        <CardText>
          <h3>Выберите задачу для студентов</h3>
          <div>
            <RadioButtonGroup name="tasks" onChange={this.onRedirect}>
              {hasByList && hasByList.items && hasByList.items.map(item => (
                <RadioButton value={item._id} label={item.name}/>
              ))
              }
            </RadioButtonGroup>
          </div>
        </CardText>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(TasksQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        themeClass: ownProps.themeClass,
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        }
      }
    })
  }),
  withRouter
)(SelectTask);