import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {CardMedia, CardText, CardActions, Card} from 'material-ui/Card';
import {QueryById} from "../query"


class MaterialLesson extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  createMarkup(html) {
    return {__html: html};
  }

  render() {
    const hasById = this.props.data.classes &&
      this.props.data.classes.byId
      && this.props.data.classes.byId;
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {

      return (
        <div>
          <CardText>
              <div dangerouslySetInnerHTML={this.createMarkup(hasById.material)} />
          </CardText>
        </div>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(QueryById, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {id: ownProps.classId}
    })
  }),
  withRouter
)(MaterialLesson);