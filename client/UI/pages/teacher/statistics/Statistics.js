import React from 'react';
import {cyan600, pink600, purple600, orange600} from 'material-ui/styles/colors';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Book from 'material-ui/svg-icons/action/book';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import Cached from 'material-ui/svg-icons/action/cached';
import InfoBox from '../../../components/statistics/InfoBox';
import NewOrders from '../../../components/statistics/NewOrders';
import MonthlySales from '../../../components/statistics/MonthlySales';
import BrowserUsage from '../../../components/statistics/BrowserUsage';
import RecentlyProducts from '../../../components/statistics/RecentlyProducts';
import StudentLecture from '../../../components/statistics/StudentLecture';
import globalStyles from '../../../components/style';
import Data from './data';

const Statistics = () => {

  return (
    <div>
      <h3 style={globalStyles.navigation}>Рабочий стол / Лекция / Общая статистика</h3>

      <div className="row">

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Book}
                   color={pink600}
                   title="Студентов на занятии"
                   value="6"
          />
        </div>


        {/*<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">*/}
          {/*<InfoBox Icon={ThumbUp}*/}
                   {/*color={cyan600}*/}
                   {/*title="Выполненные задачи"*/}
                   {/*value="23"*/}
          {/*/>*/}
        {/*</div>*/}

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Cached}
                   color={purple600}
                   title="Среднее кол-во попыток"
                   value="1,4"
          />
        </div>

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
          <InfoBox Icon={Assessment}
                   color={orange600}
                   title="Успешно выполнили"
                   value="4"
          />
        </div>
      </div>

      {/*<div className="row">*/}
        {/*<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-md m-b-15">*/}
          {/*<NewOrders data={Data.dashBoardPage.newOrders}/>*/}
        {/*</div>*/}

        {/*<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15">*/}
          {/*<MonthlySales data={Data.dashBoardPage.monthlySales}/>*/}
        {/*</div>*/}
      {/*</div>*/}

      <div className="row">
        {/*<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">*/}
          {/*<RecentlyProducts data={Data.dashBoardPage.recentProducts}/>*/}
        {/*</div>*/}

        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15 ">
          <BrowserUsage data={Data.dashBoardPage.browserUsage}/>
        </div>

        <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15">
          <StudentLecture data={Data.dashBoardPage.studentLecture}/>
        </div>
      </div>
      {/*<div className="row">*/}
        {/*<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 m-b-15">*/}
          {/*<StudentLecture data={Data.dashBoardPage.studentLecture}/>*/}
        {/*</div>*/}

      {/*</div>*/}
    </div>
  );
};

export default Statistics;