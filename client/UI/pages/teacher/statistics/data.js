import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Web from 'material-ui/svg-icons/av/web';
import {cyan600, pink600, purple600, red500, indigo600, green500} from 'material-ui/styles/colors';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

const data = {
  menus: [
    { text: 'DashBoard', icon: <Assessment/>, link: '/dashboard' },
    { text: 'Form Page', icon: <Web/>, link: '/form' },
    { text: 'Table Page', icon: <GridOn/>, link: '/table' },
    { text: 'Login Page', icon: <PermIdentity/>, link: '/login' }
  ],
  tablePage: {
    items: [
      {id: 1, name: 'Файлы', semester: '2', course: '1', academicSubject: 'Программирование'},
      {id: 2, name: 'Управление экраном', semester: '2', course: '1', academicSubject: 'Программирование'},
      {id: 3, name: 'Функции', semester: '2', course: '1', academicSubject: 'Программирование'},
      {id: 4, name: 'Динамические структуры данных', semester: '2', course: '1', academicSubject: 'Программирование'},
      {id: 5, name: 'Графы', semester: '2', course: '1', academicSubject: 'Программирование'},
      {id: 6, name: 'ООП', semester: '3', course: '2', academicSubject: 'ООП'},
      {id: 7, name: 'Отладка', semester: '1', course: '1', academicSubject: 'Программирование'},
      {id: 8, name: 'Архитектура', semester: '4', course: '2', academicSubject: 'Программирование'}
    ]
  },
  dashBoardPage: {
    recentProducts: [
      {id: 1, title: 'Книга', text: ''},
      {id: 2, title: 'Игра', text: ''},
    ],
    studentLecture: [
      {id: 1, title: 'Август Дмитрий Андреевич', text: 'КИ15-10Б'},
      {id: 2, title: 'Петров Иван Иванович', text: 'КИ17-01Б'},
      {id: 3, title: 'Антольчук Никита Сергеевич', text: 'КИ17-10Б'},
      {id: 4, title: 'Михайлович Анна Леонидовна', text: 'КИ16-08Б'},
      {id: 5, title: 'Сергеенко Анна Михайловна', text: 'КИ16-08Б'},
    ],
    monthlySales: [
      {name: 'Jan', uv: 3700},
      {name: 'Feb', uv: 3000},
      {name: 'Mar', uv: 2000},
      {name: 'Apr', uv: 2780},
      {name: 'May', uv: 2000},
      {name: 'Jun', uv: 1800},
      {name: 'Jul', uv: 2600},
      {name: 'Aug', uv: 2900},
      {name: 'Sep', uv: 3500},
      {name: 'Oct', uv: 3000},
      {name: 'Nov', uv: 2400},
      {name: 'Dec', uv: 2780}
    ],
    newOrders: [
      {pv: 2400},
      {pv: 1398},
    ],
    browserUsage: [
      {name: 'Выполнено', value: 8, color: green500},
      {name: 'Не выполнено', value: 4, color: red500,},
    ]
  }
};

export default data;