import React from 'react';
import {Link} from 'react-router-dom';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../../../components/base/PageBase';
import TasksList from './components/TasksList';

const ClassesPage = () => {

  const styles = {
    floatingActionButton: {
      margin: 0,
      top: 'auto',
      right: 20,
      bottom: 20,
      left: 'auto',
      position: 'fixed',
    },
    editButton: {
      fill: grey500
    },
    columns: {
      id: {
        width: '5%'
      },
      name: {
        width: '35%'
      },
      price: {
        width: '25%'
      },
      category: {
        width: '25%'
      },
      edit: {
        width: '5%'
      },
      view: {
        width: '5%'
      }
    }
  };

  return (
    <PageBase title="Список  задач"
              navigation="Рабочий стол / Задачи">

      <div>
        <Link to="/teacher/task/add" >
          <FloatingActionButton style={styles.floatingActionButton} backgroundColor={pink500}>
            <ContentAdd />
          </FloatingActionButton>
        </Link>

        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn>Название</TableHeaderColumn>
              <TableHeaderColumn>Предмет</TableHeaderColumn>
              <TableHeaderColumn>Занятие</TableHeaderColumn>
              <TableHeaderColumn>Редактирование</TableHeaderColumn>
              <TableHeaderColumn>Просмотр</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            <TasksList
              offset="0"
              count="10"
            />
          </TableBody>
        </Table>
      </div>
    </PageBase>
  );
};

export default ClassesPage;