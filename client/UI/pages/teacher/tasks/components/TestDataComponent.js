import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CardText from 'material-ui/Card/CardText';
import {Field} from 'redux-form';
import {TextField} from 'redux-form-material-ui'
import RaisedButton from 'material-ui/RaisedButton';

class TestDataComponent extends Component {
  static propTypes = {
    fields: PropTypes.shape(PropTypes.object).isRequired,
  };

  componentWillMount() {
    const { fields } = this.props;
    if (!fields.length) {
      fields.push({});
    }
  }

  render() {
    return (
      <div>
        <h4>Тестоые данные для проверки</h4>
        {
          this.props.fields.map((testData, index) => (
            <CardText key={`testData_${index}`}>
              <Field
                id={`inputData_${index}`}
                name={`${testData}.inputData`}
                type="text"
                component={TextField}
                hintText="Входные данные"
                fullWidth
              />
              <Field
                id={`outputData_${index}`}
                name={`${testData}.outputData`}
                type="text"
                component={TextField}
                hintText="Выходные данные"
                fullWidth
              />
              <RaisedButton label="Удалить" onClick={() => this.props.fields.remove(index)}/>
              {
                (this.props.fields.length - 1) === index &&
                <RaisedButton label="Добавить тестовые данные"
                              primary={true}
                              onClick={() => this.props.fields.push({})}
                />
              }
            </CardText>
          ))
        }
        {
          (this.props.fields.length) === 0 &&
          <RaisedButton label="Добавить тестовые данные"
                        primary={true}
                        onClick={() => this.props.fields.push({})}
          />
        }
      </div>
    )
  }
}

export default TestDataComponent;