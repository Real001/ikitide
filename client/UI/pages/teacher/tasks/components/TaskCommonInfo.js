import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Card, {CardMedia, CardText} from 'material-ui/Card';
import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class TaskCommonInfo extends Component {
  static propTypes = {
    common: PropTypes.object
  };

  static defaultProps = {
    common: {}
  };

  render() {
    return (
      <div>
        <Card style={styles.card}>
          <CardText>
            <div style={styles.title}>
              <InfoListDt title="Название"/>
              <InfoListDd title={this.props.common.name ? this.props.common.name : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Учебный предмет"/>
              <InfoListDd title={this.props.common.academicSubject ? this.props.common.academicSubject.title : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Описание"/>
              <InfoListDd title={this.props.common.description ? this.props.common.description : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Темя занятия"/>
              <InfoListDd title={this.props.common.themeClass ? this.props.common.themeClass.name : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Время на выолнение"/>
              <InfoListDd title={this.props.common.time ? `${this.props.common.time} минут` : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Входные данные"/>
              <InfoListDd title={this.props.common.inputData ? this.props.common.inputData : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Выходные данные"/>
              <InfoListDd title={this.props.common.outputData ? this.props.common.outputData : ''}/>
            </div>
            <div>
              <h4>Тестовые данные для проверки</h4>
              {this.props.common.testData && this.props.common.testData.map((item, index) => (
                <div>
                  <p>{`Тестовые данные №${index+1}`}</p>
                  <div style={styles.title}>
                    <InfoListDt title="Входные данные"/>
                    <InfoListDd title={item.inputData}/>
                  </div>
                  <div style={styles.title}>
                    <InfoListDt title="Выходные данные"/>
                    <InfoListDd title={item.outputData}/>
                  </div>
                </div>
              ))}
            </div>
          </CardText>
        </Card>
      </div>
    )
  }
}

export default TaskCommonInfo;