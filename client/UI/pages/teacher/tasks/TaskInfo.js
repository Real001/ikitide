import React, {Component} from 'react';
import {graphql} from 'react-apollo/index';
// import { withStyles } from 'material-ui/styles';
import {pipe} from 'ramda';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PageBase from '../../../components/base/PageBase';
import TaskCommonInfo from './components/TaskCommonInfo';
import {QueryById} from "./query"

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class TaskInfo extends Component {
  static propTypes = {
    history: PropTypes.object,
    data: PropTypes.object,
    dispatch: PropTypes.func
  };

  static defaultProps = {
    history: {},
    dispatch: () => {}
  };

  render() {
    const hasById = this.props.data.tasks &&
      this.props.data.tasks.byId
      && this.props.data.tasks.byId;
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return (
        <PageBase title="Информация о задаче"
                  navigation={"Рабочий стол / Задачи / " + this.props.data.tasks.byId.name}>
          <div>
            <TaskCommonInfo common={this.props.data.tasks.byId}/>
          </div>
        </PageBase>
      )
    }
  }
}

export default pipe(
  connect(),
  graphql(QueryById, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {id: ownProps.match.params.id}
    })
  }),
  withRouter
)(TaskInfo);