import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {pipe} from 'ramda';
import {Field, FieldArray, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import MenuItem from 'material-ui/MenuItem'
import {grey400} from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import {SelectField, TextField} from 'redux-form-material-ui';
import TestDataComponent from '../components/TestDataComponent';
import QueryAcademicSubject from '../../../../components/query/QueryAcademicSubject';
import QueryClasses from '../../../../components/query/QueryClasses';

const styles = {
  toggleDiv: {
    maxWidth: 300,
    marginTop: 40,
    marginBottom: 5
  },
  toggleLabel: {
    color: grey400,
    fontWeight: 100
  },
  buttons: {
    marginTop: 30,
    float: 'right'
  },
  saveButton: {
    marginLeft: 5
  }
};

const required = value => (value == null ? 'Поле обязательно для заполнения' : undefined);

class CommonCreateTask extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    formName: PropTypes.string.isRequired,
    onClickCancel: PropTypes.func,
    reset: PropTypes.func,
    submitting: PropTypes.bool,
  };

  static defaultProps = {
    handleSubmit: () => {
    },
    onClickCancel: () => {
    },
    submitting: false,
    reset: () => {
    },
  };

  constructor(props) {
    super(props);
    this.onClearForm = this.onClearForm.bind(this);
  }

  onClearForm() {
    this.props.reset();
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div>
          <Field
            id="name"
            name="name"
            type="text"
            component={TextField}
            hintText="Название"
            fullWidth
            validate={required}
          />
        </div>
        <div>
         <QueryAcademicSubject/>
        </div>
        <div>
         <QueryClasses/>
        </div>
        <div>
          <Field
            id="description"
            name="description"
            type="text"
            component={TextField}
            hintText="Описание"
            multiLine
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            id="inputData"
            name="inputData"
            type="text"
            component={TextField}
            hintText="Входные данные"
            multiLine
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            id="outputData"
            name="outputData"
            type="text"
            component={TextField}
            hintText="Выходные данные"
            multiLine
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            id="time"
            name="time"
            type="text"
            component={TextField}
            hintText="Время на выполнение задачи (минуты)"
            multiLine
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <FieldArray
            name="testData"
            component={TestDataComponent}
          />
        </div>
        <Divider/>

        <div style={styles.buttons}>
          <Link to="/teacher/tasks">
            <RaisedButton label="Отменить"/>
          </Link>

          <RaisedButton label="Сохранить"
                        style={styles.saveButton}
                        type="submit"
                        primary={true}/>
        </div>
      </form>
    )
  }
}

export default pipe(reduxForm())(CommonCreateTask);