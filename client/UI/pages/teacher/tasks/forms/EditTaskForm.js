import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CommonCreateTask from './CommonCreateTask';

class  EditTaskForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onClickCancel: PropTypes.func,
    initialValues: PropTypes.object.isRequired
  };

  static defaultProps = {
    onClickCancel: () => {
    },
  };

  render(){
    return(
      <CommonCreateTask
        onSubmit={this.props.onSubmit}
        form="EditTaskForm"
        formName="EditTaskForm"
        onClickCancel={this.props.onClickCancel}
        initialValues={this.props.initialValues}
      />
    )
  }
}

export default EditTaskForm;