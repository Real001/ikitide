import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import CommonCreateTask from './CommonCreateTask';

class  CreateTaskForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onClickCancel: PropTypes.func,
    initialValues: PropTypes.object.isRequired
  };

  static defaultProps = {
    onClickCancel: () => {
    },
  };

  render(){
    return(
      <CommonCreateTask
        onSubmit={this.props.onSubmit}
        form="CreateTaskForm"
        formName="CreateTaskForm"
        onClickCancel={this.props.onClickCancel}
      />
    )
  }
}

export default CreateTaskForm;