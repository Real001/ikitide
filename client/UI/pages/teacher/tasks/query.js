import gql from 'graphql-tag';

export const AddTaskMutation = gql`
    mutation CreateTask($input: CreateTaskInput!) {
        createTask(input: $input){
            _id
        }
    }
`;

export const UpdateTaskMutation = gql`
    mutation UpdateTask($id: ObjectId! $input: UpdateTaskInput!) {
        updateTask(_id:$id input: $input){
            _id
        }
    }
`;

export const TasksQuery = gql`
    query GetTasks($count: Int! $offset: Int!){
        tasks {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    name
                    academicSubject {
                        title
                    }
                    themeClass {
                        name
                    }
                }
            }
        }
    }
`;

export const QueryById = gql`
    query ById($id: ObjectId!) {
        tasks{
            byId(_id: $id) {
                name
                description
                themeClass{
                    name
                }
                inputData
                outputData
                academicSubject {
                    title
                }
                testData{
                    inputData
                    outputData
                }
                time
            }
        }
    }
`;