import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo/index';
import {destroy} from 'redux-form';
import {pipe} from 'ramda';
import {connect} from 'react-redux';
import {Redirect } from 'react-router-dom';
import PageBase from '../../../components/base/PageBase';
import {QueryById, UpdateTaskMutation} from "./query"
import EditTaskForm from './forms/EditTaskForm';

class TaskEdit extends Component {
  static propTypes = {
    editTask: PropTypes.func,
    data: PropTypes.shape({
      tasks: PropTypes.shape()
    }),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string
      })
    }),
    dispatch: PropTypes.func
  };

  static defaultProps = {
    editTask: () => {},
    data: {},
    match: {},
    dispatch: () => {
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      isRedirect: false,
      error: ''
    };
  }

  componentWillMount() {
    this.props.dispatch(destroy('EditTaskForm'));
  }

  onEditTask = async (event) => {
    let testData = []
    for (let i = 0; i < event.testData.length; i++) {
      testData.push({
        inputData : event.testData[i].inputData,
        outputData : event.testData[i].outputData,
      })
    }
    const res = await this.props.editTask({
      variables: {
        id: this.props.match.params.id,
        input: {
          name: event.name,
          description: event.description,
          themeClass: event.themeClass,
          academicSubject: event.academicSubject,
          inputData: event.inputData,
          outputData: event.outputData,
          testData: testData,
          time: event.time
        }
      }
    });
    this.setState({
      isRedirect: true
    });
    return res;
  };

  render() {
    if (this.state.isRedirect) {
      return (<Redirect to="/teacher/tasks" />);
    }
    if (this.props.data.loading){
      return ("Идет загрузка страницы, подождите")
    }
    const taskValues =  this.props.data &&  this.props.data.tasks &&  this.props.data.tasks.byId;
    return (
      <PageBase title="Редактирование задачи"
                navigation="Рабочий стол / Задачи / Редактирование">
      <EditTaskForm
        onSubmit={this.onEditTask}
        initialValues={taskValues}/>
        onClickCancel={() => this.setState({isRedirect: true})}
      </PageBase>
    );
  }
}

export default pipe(
  graphql(UpdateTaskMutation, {
    name: 'editTask'
  }),
  graphql(QueryById, {
    options: ownProps => ({
      variables: {
        id: ownProps.match.params.id
      }
    })
  }),
  connect(state => state),
)(TaskEdit);