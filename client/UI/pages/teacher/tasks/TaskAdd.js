import React, {PureComponent} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {pipe} from 'ramda';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {grey400} from 'material-ui/styles/colors';
import PageBase from '../../../components/base/PageBase';
import CreateTaskForm from './forms/CreateTaskForm';
import {AddTaskMutation} from "./query"

const styles = {
  toggleDiv: {
    maxWidth: 300,
    marginTop: 40,
    marginBottom: 5
  },
  toggleLabel: {
    color: grey400,
    fontWeight: 100
  },
  buttons: {
    marginTop: 30,
    float: 'right'
  },
  saveButton: {
    marginLeft: 5
  }
};

class TaskAdd extends PureComponent {
  static propTypes = {
    history: PropTypes.shape,
    addClass: PropTypes.func.isRequired
  };

  static defaultProps = {
    history: {}
  };

  constructor(props) {
    super(props);
    this.state = {
      isRedirect: false
    };
  }

  onCreateTask = async (event) => {
    const res = await this.props.addTask({
      variables: {
        input: {
          name: event.name,
          description: event.description,
          themeClass: event.themeClass,
          academicSubject: event.academicSubject,
          inputData: event.inputData,
          outputData: event.outputData,
          testData: event.testData,
          time: event.time
        }
      }
    });
    this.setState({
      isRedirect: true
    });
    return res;
  };

  render() {
    if (this.state.isRedirect) {
      return (<Redirect to="/teacher/tasks" />);
    }

    return(
      <PageBase title="Добавить новую задачу"
                navigation="Рабочий стол / Задачи / Добавить">

        <CreateTaskForm onSubmit={this.onCreateTask} />

      </PageBase>
    )
  }
}

export default pipe( connect(),
  graphql(AddTaskMutation, {
    name: 'addTask'
  }),
  withRouter)(TaskAdd);