import React, {Component} from 'react';
import {pipe} from 'ramda';
import {withRouter, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import {grey200, grey500} from 'material-ui/styles/colors';
import ContentPaste from 'material-ui/svg-icons/content/content-paste';
import CircularProgress from 'material-ui/CircularProgress';
import {ClassesQuery} from "../query"

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  editButton: {
    fill: grey500
  },
  viewButton: {
    fill: grey500
  },
  columns: {
    id: {
      width: '10%'
    },
    name: {
      width: '30%'
    },
    price: {
      width: '5%'
    },
    category: {
      width: '5%'
    },
    academicSubject: {
      width: '10%'
    },
    edit: {
      width: '10%'
    },
    view: {
      width: '10%'
    },
  },
  progress: {
    margin: 10,
  },
};


class ClassesList extends Component {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func
    }),
    onTotalCallBack: PropTypes.func,
    data: PropTypes.shape({
      classes: PropTypes.shape()
    })
  };

  static defaultProps = {
    history: {},
    onTotalCallBack: () => {
    },
    data: {}
  };

  render() {
    if (this.props.data.loading) {
      return(<p>Loading....</p>)
    } else {
      return(
        <div>
          {this.props.data.classes.list.items.map(item => (
            <TableRow key={item._id}>
              <TableRowColumn>{item.name}</TableRowColumn>
              <TableRowColumn>{item.semester}</TableRowColumn>
              <TableRowColumn>{item.course}</TableRowColumn>
              <TableRowColumn>{item.academicSubject && item.academicSubject.title}</TableRowColumn>
              <TableRowColumn>
                <Link className="button" to={"/teacher/classes/edit/" + item._id}>
                  <FloatingActionButton zDepth={0}
                                        mini={true}
                                        backgroundColor={grey200}
                                        iconStyle={styles.editButton}>
                    <ContentCreate  />
                  </FloatingActionButton>
                </Link>
              </TableRowColumn>
              <TableRowColumn>
                <Link className="button" to={"/teacher/classes/view/" + item._id}>
                  <FloatingActionButton zDepth={0}
                                        mini={true}
                                        backgroundColor={grey200}
                                        iconStyle={styles.viewButton}>
                    <ContentPaste  />
                  </FloatingActionButton>
                </Link>
              </TableRowColumn>
            </TableRow>
            )
          )}
        </div>
      )
    }
  }
}

export default pipe(
  withRouter,
  graphql(ClassesQuery, {
    options: ownProps => ({
      fetchPolicy: 'network-only',
      variables: {
        offset: 0,
        count: 10,
        sortOptions: {
          createDate: -1
        }
      }
    })
  })
)(ClassesList);