import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Card, {CardMedia, CardText} from 'material-ui/Card';
import {InfoListDd, InfoListDt} from "../../../../components/infolist/InfoList"

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class ClassesCommonInfo extends Component {
  static propTypes = {
    common: PropTypes.object
  };

  static defaultProps = {
    common: {}
  };

  render() {
    return (
      <div>
        <Card style={styles.card}>
          <CardText>
            <div style={styles.title}>
              <InfoListDt title="Название"/>
              <InfoListDd title={this.props.common.name ? this.props.common.name : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Описание"/>
              <InfoListDd title={this.props.common.description ? this.props.common.description : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Семестр"/>
              <InfoListDd title={this.props.common.semester ? this.props.common.semester : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Курс"/>
              <InfoListDd title={this.props.common.course ? this.props.common.course : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Учебный предмет"/>
              <InfoListDd title={this.props.common.academicSubject ? this.props.common.academicSubject.title : ''}/>
            </div>
            <div style={styles.title}>
              <InfoListDt title="Материал занятия"/>
              <InfoListDd title={this.props.common.material ? this.props.common.material : ''}/>
            </div>
          </CardText>
        </Card>
      </div>
    )
  }
}

export default ClassesCommonInfo