import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import CommonCreateClass from './CommonCreateClass';

class  EditCassForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onClickCancel: PropTypes.func,
    initialValues: PropTypes.object.isRequired
  };

  static defaultProps = {
    onClickCancel: () => {
    },
  };

  render(){
    return(
      <CommonCreateClass
        onSubmit={this.props.onSubmit}
        form="EditClassForm"
        formName="EditClassForm"
        onClickCancel={this.props.onClickCancel}
        initialValues={this.props.initialValues}
      />
    )
  }
}

export default EditCassForm;