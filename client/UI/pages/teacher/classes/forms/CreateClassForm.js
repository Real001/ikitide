import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import CommonCreateClass from './CommonCreateClass';

class  CreateClassForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onClickCancel: PropTypes.func,
    initialValues: PropTypes.object.isRequired
  };

  static defaultProps = {
    onClickCancel: () => {
    },
  };

  render(){
    return(
      <CommonCreateClass
        onSubmit={this.props.onSubmit}
        form="CreateClassForm"
        formName="CreateClassForm"
        onClickCancel={this.props.onClickCancel}
      />
    )
  }
}

export default CreateClassForm;