import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {pipe} from 'ramda';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {grey400} from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import renderEditorField from "../../../../components/textEditor/renderField";
import {TextField,} from 'redux-form-material-ui'
import QueryAcademicSubject from '../../../../components/query/QueryAcademicSubject';

const styles = {
  toggleDiv: {
    maxWidth: 300,
    marginTop: 40,
    marginBottom: 5
  },
  toggleLabel: {
    color: grey400,
    fontWeight: 100
  },
  buttons: {
    marginTop: 30,
    float: 'right'
  },
  saveButton: {
    marginLeft: 5
  }
};

const required = value => (value == null ? 'Поле обязательно для заполнения' : undefined);

class CommonCreateClass extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    formName: PropTypes.string.isRequired,
    onClickCancel: PropTypes.func,
    reset: PropTypes.func,
    submitting: PropTypes.bool,
  };

  static defaultProps = {
    handleSubmit: () => {
    },
    onClickCancel: () => {
    },
    submitting: false,
    reset: () => {
    },
  };

  constructor(props) {
    super(props);
    this.onClearForm = this.onClearForm.bind(this);
  }

  onClearForm() {
    this.props.reset();
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div>
          <Field
            name="name"
            type="text"
            component={TextField}
            hintText="Название"
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            name="description"
            type="text"
            component={TextField}
            hintText="Описание"
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            name="semester"
            type="text"
            component={TextField}
            hintText="Семестр"
            fullWidth
            validate={required}
          />
        </div>
        <div>
          <Field
            name="course"
            type="text"
            component={TextField}
            hintText="Курс"
            fullWidth
            validate={required}
          />
        </div>
        <div>
         <QueryAcademicSubject/>
        </div>
        <div>
          <Field
            name="material"
            component={renderEditorField}
          />
        </div>
        <Divider/>

        <div style={styles.buttons}>
          <Link to="/teacher/classes">
            <RaisedButton label="Отменить"/>
          </Link>

          <RaisedButton label="Сохранить"
                        style={styles.saveButton}
                        type="submit"
                        primary={true}/>
        </div>
      </form>
    )
  }
}

export default pipe(reduxForm())(CommonCreateClass);