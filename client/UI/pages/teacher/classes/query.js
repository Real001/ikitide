import gql from 'graphql-tag';

export const ClassesQuery = gql`
    query GetClasses($count: Int! $offset: Int!){
        classes {
            list(listOptions:{offset: $offset count:$count}){
                meta{
                    total
                }
                items{
                    _id
                    name
                    semester
                    course
                    academicSubject{
                        _id
                        title
                    }
                }
            }
        }
    }
`;

export const QueryById = gql`
  query ById($id: ObjectId!) {
      classes{
          byId(_id: $id) {
              name
              description
              semester
              course
              academicSubject{
                  title
                  _id
              }
              material
          }
      }
  }
`;
export const QueryByIdEdit = gql`
    query ById($id: ObjectId!) {
        classes{
            byId(_id: $id) {
                name
                description
                semester
                course
                academicSubject{
                    label: title
                    value: _id
                }
                material
            }
        }
    }
`;

export const AddClassMutation = gql`
    mutation CreateClass($input: CreateClassInput!) {
        createClass(input: $input){
            _id
        }
    }
`;

export const EditClassMutation = gql`
    mutation UpdateClass($id: ObjectId! $input: UpdateClassInput!) {
        updateClass(_id:$id input: $input){
            _id
        }
    }
`;

export const GET_ACADEMIC_SUBJECT = gql`
    query GetAcademicSubject($offset: Int! $count: Int!)
    {
        academicSubjects{
            list(listOptions: {offset:$offset, count:$count}){
                items{
                    _id
                    title
                }
            }
        }
    }
`;