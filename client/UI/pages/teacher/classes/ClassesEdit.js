import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo/index';
import {destroy} from 'redux-form';
import {pipe} from 'ramda';
import {connect} from 'react-redux';
import {Redirect } from 'react-router-dom';
import PageBase from '../../../components/base/PageBase';
import {QueryByIdEdit, EditClassMutation} from "./query"
import EditClassForm from './forms/EditClassForm';

class ClassesEdit extends Component {
  static propTypes = {
    editClass: PropTypes.func,
    data: PropTypes.shape({
      classes: PropTypes.shape()
    }),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string
      })
    }),
    dispatch: PropTypes.func
  };

  static defaultProps = {
    editClass: () => {},
    data: {},
    match: {},
    dispatch: () => {
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      isRedirect: false,
      error: ''
    };
  }

  componentWillMount() {
    this.props.dispatch(destroy('EditClassForm'));
  }

  onUpdateClass = async (event) => {
    try {
      const res = await this.props.editClass({
        variables: {
          id: this.props.match.params.id,
          input: {
            name: event.name,
            description: event.description,
            semester: event.semester,
            course: event.course,
            academicSubject: event.academicSubject,
            material: event.material
          }
        }
      });
      this.setState({
        isRedirect: true
      });
      return res;
    } catch (error) {
      this.setState({
        error: error
      });
    }
  };


  render() {
    if (this.state.isRedirect) {
      return (<Redirect to="/teacher/classes" />);
    }
    if (this.props.data.loading){
      return ("Идет загрузка страницы, подождите")
    }
    const classValues =  this.props.data &&  this.props.data.classes &&  this.props.data.classes.byId;
    return (
      <PageBase title="Редактирование занятия"
                navigation="Рабочий стол / Занятия / Редактирование">
        {this.state.error ? this.state.error : ''}
        <EditClassForm
          onSubmit={this.onUpdateClass}
          onClickCancel={() => this.setState({isRedirect: true})}
          initialValues={classValues}
        />

      </PageBase>
    );
  }
}

export default pipe(
  graphql(EditClassMutation, {
    name: 'editClass'
  }),
  graphql(QueryByIdEdit, {
    options: ownProps => ({
      variables: {
        id: ownProps.match.params.id
      }
    })
  }),
  connect(state => state),
)(ClassesEdit);