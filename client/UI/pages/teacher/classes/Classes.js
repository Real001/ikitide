import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {pipe} from 'ramda';
import PropTypes from 'prop-types';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../../../components/base/PageBase';
import ClassesList from './components/ClassesList';
import Data from './data';

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  editButton: {
    fill: grey500
  },
  columns: {
    id: {
      width: '10%'
    },
    name: {
      width: '30%'
    },
    price: {
      width: '5%'
    },
    category: {
      width: '5%'
    },
    academicSubject: {
      width: '10%'
    },
    edit: {
      width: '10%'
    },
    view: {
      width: '10%'
    }
  }
};


class Classes extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    history: PropTypes.shape({
      push: PropTypes.func
    }),
    location: PropTypes.shape
  };

  static defaultProps = {
    dispatch: () => {},
    history: {},
    location: {}
  };
  render() {
    return (
      <PageBase title="Список занятий"
                navigation="Рабочий стол / Занятия">

        <div>
          <Link to="/teacher/classes/add" >
            <FloatingActionButton style={styles.floatingActionButton} backgroundColor={pink500}>
              <ContentAdd />
            </FloatingActionButton>
          </Link>

          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn>Название</TableHeaderColumn>
                <TableHeaderColumn>Семестр</TableHeaderColumn>
                <TableHeaderColumn>Курс</TableHeaderColumn>
                <TableHeaderColumn>Предмет</TableHeaderColumn>
                <TableHeaderColumn>Редактирование</TableHeaderColumn>
                <TableHeaderColumn>Просмотр</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
              <ClassesList
                offset="0"
                count="10"
                // history={this.props.history}
              />
            </TableBody>
          </Table>
        </div>
      </PageBase>
    );
  }
}

export default pipe(withRouter)(Classes);