import React, {PureComponent} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {pipe} from 'ramda';
import {connect} from 'react-redux';
import {graphql} from 'react-apollo/index';
import {grey400} from 'material-ui/styles/colors';
import {AddClassMutation} from "./query"
import PageBase from '../../../components/base/PageBase';
import CreateClassForm from './forms/CreateClassForm';

const styles = {
  toggleDiv: {
    maxWidth: 300,
    marginTop: 40,
    marginBottom: 5
  },
  toggleLabel: {
    color: grey400,
    fontWeight: 100
  },
  buttons: {
    marginTop: 30,
    float: 'right'
  },
  saveButton: {
    marginLeft: 5
  }
};

class ClassesAdd extends PureComponent {
  static propTypes = {
    history: PropTypes.shape,
    addClass: PropTypes.func.isRequired
  };

  static defaultProps = {
    history: {}
  };

  constructor(props) {
    super(props);
    this.state = {
      isRedirect: false
    };
  }

  onCreateClass = async (event) => {
    const res = await this.props.addClass({
      variables: {
        input: {
          name: event.name,
          description: event.description,
          semester: event.semester,
          course: event.course,
          academicSubject: event.academicSubject,
          material: event.material
        }
      }
    });
    this.setState({
      isRedirect: true
    });
    return res;
  };

  render() {
    if (this.state.isRedirect) {
      return (<Redirect to="/teacher/classes" />);
    }

    return(
      <PageBase title="Добавить новое занятие"
                navigation="Рабочий стол / Занятия / Добавить">
        <CreateClassForm onSubmit={this.onCreateClass} onClickCancel={() => this.setState({isRedirect: true})}/>

      </PageBase>
    )
  }

}

// const ClassesAdd = () => {
//
//   return (
//     <PageBase title="Добавить новое занятие"
//               navigation="Рабочий стол / Занятия / Добавить">
//       <form>
//
//         <TextField
//           hintText="Название"
//           floatingLabelText="Название"
//           fullWidth={true}
//         />
//
//         <SelectField
//           floatingLabelText="Семестр"
//           value=""
//           fullWidth={true}>
//           <MenuItem key={0} primaryText="1"/>
//           <MenuItem key={1} primaryText="2"/>
//         </SelectField>
//
//         <SelectField
//           floatingLabelText="Курс"
//           value=""
//           fullWidth={true}>
//           <MenuItem key={0} primaryText="1"/>
//           <MenuItem key={1} primaryText="2"/>
//           <MenuItem key={0} primaryText="3"/>
//           <MenuItem key={1} primaryText="4"/>
//           <MenuItem key={0} primaryText="5"/>
//           <MenuItem key={1} primaryText="6"/>
//         </SelectField>
//
//         <TextField
//           hintText="Описание"
//           floatingLabelText="Описание"
//           fullWidth={true}
//         />
//
//         <DatePicker
//         hintText="Expiration Date"
//         floatingLabelText="Expiration Date"
//         fullWidth={true}/>
//
//         <div style={styles.toggleDiv}>
//         <Toggle
//         label="Disabled"
//         labelStyle={styles.toggleLabel}
//         />
//         </div>
//
//         <Divider/>
//
//         <div style={styles.buttons}>
//           <Link to="/teacher/classes">
//             <RaisedButton label="Отменить"/>
//           </Link>
//
//           <RaisedButton label="Сохранить"
//                         style={styles.saveButton}
//                         type="submit"
//                         primary={true}/>
//         </div>
//       </form>
//       <CreateClassForm onSubmit={this.onCreateClass} onClickCancel={() => this.setState({isRedirect: true})}/>
//     </PageBase>
//   );
// };


export default pipe( connect(),
  graphql(AddClassMutation, {
    name: 'addClass'
  }),
  withRouter)(ClassesAdd);