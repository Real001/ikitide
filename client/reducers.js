import { combineReducers } from 'redux';
import login from './UI/redux/reducers/login';
import lesson from './UI/redux/reducers/lesson';

export default combineReducers({login, lesson});