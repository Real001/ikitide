import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {createHttpLink} from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';

const httpLink = createHttpLink({
  uri: '/graphql',
  credentials: 'same-origin'
});

const middlewareAuthToken = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: null
    }
  });
  return forward(operation);
});

let link = middlewareAuthToken.concat(httpLink);

const errorLink = onError(({ networkError }) => {
  if (networkError.statusCode === 401) {
    console.log('401');
  }
});

link = errorLink.concat(link);

const cache = new InMemoryCache().restore(window.APOLLO_STATE);
const client = new ApolloClient({link, cache});
export default client;