import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import {createLogger} from 'redux-logger';
import reducer from './reducers';
import DevTools from './DevTools';

export default function (initial, {log = false} = {}) {
  const middlewares = [];
  if (log) {
    const logger = createLogger();
    middlewares.push(logger);
  }

  const store = createStore(
    combineReducers({
      pages: reducer,
      form: reduxFormReducer
    }),
    initial,
    compose(
      applyMiddleware(...middlewares),
      DevTools.instrument(),
    ),
  );

  /* eslint-disable global-require */
  if (module.hot) {
    module.hot.accept('./reducers', () => store.replaceReducer(require('./reducers').default));
  }
  /* eslint-enable global-require */

  return store;
}