'use strict';
const groups = require('../common/data/groups');

module.exports = {

  async up(db, next) {
    // TODO write your migration here
    await db.collection('groups').insertMany(groups);
    next();
  },

  down(db, next) {
    // TODO write the statements to rollback your migration (if possible)
    next();
  }

};