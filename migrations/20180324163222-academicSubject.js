'use strict';
const academicSubject = require('../common/data/academicSubject');
module.exports = {

  async up(db, next) {
    // TODO write your migration here
    await db.collection('academicSubjects').insertMany(academicSubject);
    next();
  },

  down(db, next) {
    // TODO write the statements to rollback your migration (if possible)
    next();
  }
}
