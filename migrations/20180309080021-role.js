'use strict';
const role = require('../common/data/role');
module.exports = {

  async up(db, next) {
    // TODO write your migration here
    await db.collection('role').insertMany(role);
    next();
  },

  down(db, next) {
    // TODO write the statements to rollback your migration (if possible)
    next();
  }

};