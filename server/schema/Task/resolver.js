const {createError} = require('apollo-errors');
const Task = require('./schema');
const db = require('../../models/task');

const resolver = {
  Tasks: {
    async byId(obj, {_id}) {
      let task = await db.findOne({_id: _id}).populate('themeClass').populate('academicSubject');
      return task;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.name !== 'undefined') {
          filter.name = new RegExp(`.*${findOptions.name}.*`, 'i');
        }
        if (typeof findOptions.academicSubject !== 'undefined') {
          filter.academicSubject = findOptions.academicSubject;
        }
        if (typeof findOptions.themeClass !== 'undefined') {
          filter.themeClass = findOptions.themeClass;
        }
      }
      const items = await db.find(filter).populate('themeClass').populate('academicSubject').limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  },
  Mutation: {
    async createTask(obj, {input}) {
      const {name, academicSubject, description, themeClass, inputData, outputData, testData, time} = input;
      const task = {name, academicSubject, description, themeClass, inputData, outputData, testData, time};
      const result = await new db(task).save();
      return {_id: result._id};
    },
    async updateTask(obj, {_id, input}) {
      const {name, academicSubject, description, themeClass, inputData, outputData} = input;
      const data = db.findById(_id);
      // if (!data) { throw new UpdateUserNotFoundError(); }
      const update = {
        updateDate: new Date(),
        name, academicSubject, description, themeClass, inputData, outputData
      };
      await db.updateOne({_id: _id}, update);
      return  {_id: _id};
    }
  }
};

module.exports = [{resolver}, Task];