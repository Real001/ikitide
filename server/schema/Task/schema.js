const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');
const AcademicSubject = require('../reference/academicSubject/schema');
const Classes = require('../Class/schema');

const schema = `
    type Task {
    _id: ObjectId!
    name: String
    academicSubject: AcademicSubject
    description: String
    themeClass: Class
    inputData: String
    outputData: String
    testData: [TestData]
    time: Int
  }
  
  type TestData {
    inputData: String
    outputData: String
  }
  input TestDataInput {
    inputData: String
    outputData: String
  }
  
  type Tasks {
    byId(_id: ObjectId!): Task
    list(listOptions: ListOptions!, findOptions: TasksFindOptions): TasksList
  }
  
  input TasksFindOptions {
    name: String
    academicSubject: String
    themeClass: ObjectId
  }
  type TasksList {
    meta: ListMeta
    items: [Task]
  }
  
  extend type Mutation{
    createTask(input: CreateTaskInput!): CreateTaskResult
    updateTask(_id:ObjectId input: UpdateTaskInput!): CreateTaskResult
  }
  type CreateTaskResult{
    _id: ObjectId!
  }
  
  input CreateTaskInput {
    name: String
    description: String
    academicSubject: ObjectId
    themeClass: ObjectId
    inputData: String
    outputData: String
    time: Int
    testData: [TestDataInput]
  }
  
  input UpdateTaskInput {
    name: String
    description: String
    themeClass: ObjectId
    inputData: String
    outputData: String
    time: Int
    testData: [TestDataInput]
     academicSubject: ObjectId
  }
`;

module.exports = [{schema}, ListMeta, ListOptions, AcademicSubject, Classes];