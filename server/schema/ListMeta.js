const schema = `
  # Метаинформация списка
  type ListMeta {
    # Количество элементов в результате выборки
    count: Int!
    # Общее количество элементов в списке(без учета count и offset)
    total: Int!
    # Смещение в списке
    offset: Int!
  }
`;

module.exports =  [{schema}];