const schema = `
  # Опции запроса к спискам 
  input ListOptions {
    # Смещение в списке
    offset: Int!
    # Количество элементов
    count: Int!
  }
`;

module.exports =  [{schema}];