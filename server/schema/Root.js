const GraphQLDate = require ('./scalars/GraphQLDate');
const GraphQLObjectId =require('./scalars/GraphQLObjectId');
const Users = require('./User/resolver');
const Classes = require('./Class/resolver');
const Tasks = require('./Task/resolver');
const AcademicSubjects = require('./reference/academicSubject/resolver');
const Groups = require('./reference/group/resolver');
const ExecutableTasks = require('./executableTasks/resolver');
const Auth = require('./Auth/resolver');
const StartLessons = require('./StartLessons/resolver');
const CompletedTasks = require('./CompletedTasks/resolver');

const schema = `
  scalar Date
  scalar ObjectId

  # Корневой элемент запросов
  type Query {
   users: Users
   classes: Classes
   tasks: Tasks
   academicSubjects: AcademicSubjects
   groups: Groups
   executableTasks: ExecutableTasks
   startLessons: StartLessons
   completedTasks: CompletedTasks
  }

  # Корневой тип мутаций
  type Mutation{
    hello: String!
  }

  # Корневой элемент схемы
  schema {
    query: Query
    mutation: Mutation
  }
`;

const resolver = {
  Date: GraphQLDate,
  ObjectId: GraphQLObjectId,
  Query: {
    users: () => ({}),
    classes: () => ({}),
    tasks: () => ({}),
    academicSubjects: () => ({}),
    groups: () => ({}),
    executableTasks: () => ({}),
    startLessons: () => ({}),
    completedTasks: () => ({})
  }
};

module.exports = [{schema, resolver}, Users, Classes, Tasks, AcademicSubjects, Groups, ExecutableTasks, Auth, StartLessons,
CompletedTasks];
