const {createError} = require('apollo-errors');
const StartLesson = require('./schema');
const db = require('../../models/startLesson');

const resolver = {
  StartLessons: {
    async byId(obj, {_id}) {
      let startLesson = await db.find({_id}).populate('groups');
      return startLesson;
    },
    async byGroup(obj, {group}) {
      let startLesson =  await db.findOne({groups: group}).populate('academicSubject').populate('classes');
      return startLesson;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.group !== 'undefined') {
          filter.group = new RegExp(`.*${findOptions.group}.*`, 'i');
        }
      }
      const items = await db.find(filter).populate('groups', 'academicSubjects', 'classes').limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  },
  Mutation: {
    async createStartLesson(obj, {input}) {
      const {groups, academicSubject, classes} = input;
      const startLesson = {groups, academicSubject, classes};
      const result = await new db(startLesson).save();
      return {_id: result._id};
    },
    async deleteStartLesson(obj, {id}) {
     await db.remove({_id: id});
     return {_id: id};
    }
  }
};

module.exports = [{resolver}, StartLesson];