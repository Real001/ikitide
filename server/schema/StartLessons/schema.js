const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');
const AcademicSubject = require('../reference/academicSubject/schema');
const Classes = require('../Class/schema');
const Group = require('../reference/group/schema');

const schema = `
  type StartLesson {
    _id: ObjectId!
    groups: [Group]
    academicSubject: AcademicSubject
    classes: Class
    created: Date
  }
  
  type StartLessons {
    byId(_id: ObjectId!): StartLesson
    byGroup(group: String!): StartLesson
    list(listOptions: ListOptions!, findOptions: StartLessonsFindOptions): StartLessonsList
  }
  
  input StartLessonsFindOptions {
    group: String
  }
  
  type StartLessonsList {
    meta: ListMeta
    items: [StartLesson]
  }
  
  extend type Mutation{
    createStartLesson(input: CreateStartLessonInput!): CreateStartLessonResult
    deleteStartLesson(id: ObjectId!): CreateStartLessonResult
  }
  
  type CreateStartLessonResult{
    _id: ObjectId!
  }
  
  input CreateStartLessonInput {
    groups: [String]
    academicSubject: ObjectId
    classes: ObjectId
  }
`;

module.exports = [{schema}, ListMeta, ListOptions, AcademicSubject, Classes, Group];