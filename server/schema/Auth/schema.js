const schema = `
  extend type Mutation{
    createTokenByLogin(input: CreateTokenByLoginInput!): CreateTokenResult
  }

  input CreateTokenByLoginInput {
    login: String!
    password: String!
  }

  type CreateTokenResult {
    token: String
    error: String
  }
`;

module.exports = [{schema}];