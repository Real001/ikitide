const bcrypt = require('bcrypt');
const crypto  = require('crypto');
const Buffer  = require('buffer/').Buffer;
const ObjectId =  require('mongodb').ObjectId;

const schema = require('./schema');
const db = require('../../models/users');

const generateUniqueToken = (tokenLength) => {
  const token = ObjectId().toString('base64');
  const u = Buffer.from(token, 'base64');
  const randomLength = tokenLength - u.length;
  if (randomLength <= 0) { return token; }
  const buf = crypto.randomBytes(randomLength);
  const resBuf = Buffer.concat([buf, u]);
  return resBuf.toString('hex');
};

const resolver = {
  Mutation: {
    async createTokenByLogin(obj, {input}) {
      const user = await db.findOne({login: input.login});
      if (!user) {
        const error = 'Неправильно введен логин';
        return {error};
      }
      const hashedPassword = await bcrypt.hash(input.password, user.salt);
      if (user.password !== hashedPassword) {
        const error = 'Неправильно введен пароль';
        return {error};
      }
      const token = generateUniqueToken(256);
      await db.updateOne({_id: user._id}, {$set: {token: token}});
      return {token};
    }
  }
};

module.exports = [{resolver}, schema];