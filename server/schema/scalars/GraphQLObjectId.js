const {GraphQLScalarType} = require('graphql');
const {Kind}= require('graphql/language');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports = new GraphQLScalarType({
  name: 'ObjectId',
  description: 'Mongodb ObjectId type',
  serialize: value => value.toJSON(),
  parseValue: value => new ObjectId(value),
  parseLiteral: (ast) => {
    return ast.kind === Kind.STRING ? new ObjectId(ast.value) : null;
  }
});