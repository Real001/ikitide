const {GraphQLScalarType} = require('graphql');
const {Kind} =  require('graphql/language');

module.exports  = new GraphQLScalarType({
  name: 'Date',
  description: 'Date type',
  serialize: value => value.toJSON(),
  parseValue: value => new Date(value),
  parseLiteral: (ast) => {
    return ast.kind === Kind.STRING ? new Date(ast.value) : null;
  }
});