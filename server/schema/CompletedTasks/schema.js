const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');
const Task = require('../Task/schema');
const Classes = require('../Class/schema');
const Users = require('../User/schema');

const schema = `
  type CompletedTask {
    _id: ObjectId!
    user: Users
    lesson: Classes
    task: Task
    test: Int
    code: String
    created: Date
  }
  
  type CompletedTasks {
    byId(_id: ObjectId!): CompletedTask
    list(listOptions: ListOptions!, findOptions: CompletedTaskFindOptions): CompletedTasksList
  }
  
  input CompletedTaskFindOptions {
    lesson: String
    task: String
  }
  
  type CompletedTasksList {
    meta: ListMeta
    items: [CompletedTask]
  }
  
  extend type Mutation{
    createCompletedTask(input: CreateCompletedTaskInput!): CreateCompletedTaskResult
  }
  
  type CreateCompletedTaskResult{
    _id: ObjectId!
  }
  
  input CreateCompletedTaskInput {
    user: ObjectId!
    lesson: ObjectId
    task: ObjectId
    test: Int
    code: String!
  }
`;

module.exports = [{schema}, ListMeta, ListOptions, Task, Classes, Users];