const {createError} = require('apollo-errors');
const CompletedTask = require('./schema');
const db = require('../../models/completedTasks');

const resolver = {
  CompletedTasks: {
    async byId(obj, {_id}) {
      const completedTask = await db.findOne({_id: _id}).populate('classes', 'tasks');
      return completedTask;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.group !== 'undefined') {
          filter.lesson = new RegExp(`.*${findOptions.lesson}.*`, 'i');
        }
      }
      const items = await db.find(filter).populate('classes', 'tasks').limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  },
  Mutation: {
    async createCompletedTask(obj, {input}) {
      const {lesson, task, test, code, user} = input;
      const completedTask = {lesson, task, test, code, user};
      const result = await new db(completedTask).save();
      return {_id: result._id};
    }
  }
};

module.exports = [{resolver}, CompletedTask];