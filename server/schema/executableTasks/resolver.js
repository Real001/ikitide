const {createError} = require('apollo-errors');
const ExecutableTask = require('./schema');
const db = require('../../models/executableTasks');

const resolver = {
  ExecutableTasks: {
    async byId(obj, {_id}) {
      let executableTask = await db.findOne({_id: _id}).populate('groups', 'academicSubjects');
      return executableTask;
    },
    async byStartLesson(obj, {startLesson}) {
      let executableTask = await db.findOne({startLesson: startLesson}).populate('task');
      return executableTask;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.group !== 'undefined') {
          filter.group = new RegExp(`.*${findOptions.group}.*`, 'i');
        }
      }
      const items = await db.find(filter).populate('groups', 'academicSubjects', 'classes', 'tasks').limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  },
  Mutation: {
    async createExecutableTask(obj, {input}) {
      const {startLesson, task, timeTask} = input;
      const executableTask = {startLesson, task, timeTask};
      const result = await new db(executableTask).save();
      return {_id: result._id};
    },
    async deleteExecutableTask(obj, {id}) {
     await db.remove({_id: id});
     return {_id: id};
    }
  }
};

module.exports = [{resolver}, ExecutableTask];