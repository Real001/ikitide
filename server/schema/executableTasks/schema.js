const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');
const Task = require('../Task/schema');
const StartLesson = require('../StartLessons/schema')

const schema = `
  type ExecutableTask {
    _id: ObjectId!
    startLesson: StartLesson
    task: Task
    timeTask: String
    created: Date
  }
  
  type ExecutableTasks {
    byId(_id: ObjectId!): ExecutableTask
    byStartLesson(startLesson: ObjectId!): ExecutableTask
    list(listOptions: ListOptions!, findOptions: ExecutableTaskFindOptions): ExecutableTasksList
  }
  
  input ExecutableTaskFindOptions {
    group: String
  }
  
  type ExecutableTasksList {
    meta: ListMeta
    items: [ExecutableTask]
  }
  
  extend type Mutation{
    createExecutableTask(input: CreateExecutableTaskInput!): CreateExecutableTaskResult
    deleteExecutableTask(id: ObjectId!): CreateExecutableTaskResult
  }
  
  type CreateExecutableTaskResult{
    _id: ObjectId!
  }
  
  input CreateExecutableTaskInput {
    startLesson: ObjectId
    task: ObjectId
    timeTask: String
    created: Date
  }
`;

module.exports = [{schema}, ListMeta, ListOptions, Task, StartLesson];