const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');
const AcademicSubject = require('../reference/academicSubject/schema');

const schema = `
    type Class {
    _id: ObjectId!
    name: String
    description: String
    semester: Int
    course: Int
    academicSubject: AcademicSubject
    material: String
  }
  
  type Classes {
    byId(_id: ObjectId!): Class
    list(listOptions: ListOptions!, findOptions: ClassesFindOptions): ClassesList
  }
  
  input ClassesFindOptions {
    name: String
    semester: Int
    course: Int
  }
  type ClassesList {
    meta: ListMeta
    items: [Class]
  }
  
  extend type Mutation{
    createClass(input: CreateClassInput!): CreateClassResult
    updateClass(_id:ObjectId input: UpdateClassInput!): CreateClassResult
  }
  type CreateClassResult{
    _id: ObjectId!
  }
  
  input CreateClassInput {
    name: String
    description: String
    semester: Int
    course: Int
    academicSubject: ObjectId
    material: String
  }
  
  input UpdateClassInput {
    name: String
    description: String
    semester: Int
    course: Int
    academicSubject: ObjectId
    material: String
  }
`;

module.exports = [{schema}, ListMeta, ListOptions, AcademicSubject];