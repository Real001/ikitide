const {createError} = require('apollo-errors');
const Class = require('./schema');
const db = require('../../models/classes');

const resolver = {
  Classes: {
    async byId(obj, {_id}) {
      let classes = await db.findOne({_id: _id}).populate('academicSubject');
      return classes;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.name !== 'undefined') {
          filter.name = new RegExp(`.*${findOptions.name}.*`, 'i');
        }
        if (typeof findOptions.semester !== 'undefined') {
          filter.semester = findOptions.semester;
        }
        if (typeof findOptions.course !== 'undefined') {
          filter.course =findOptions.course;
        }
        if (typeof findOptions.academicSubject !== 'undefined') {
          filter.academicSubject = new RegExp(`.*${findOptions.academicSubject}.*`, 'i');
        }
      }
      const items = await db.find(filter).populate('academicSubject').limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  },
  Mutation: {
    async createClass(obj, {input}) {
      const {name, description, semester, course, academicSubject, material} = input;
      const classes = {name, description, semester, course, academicSubject, material};
      const result = await new db(classes).save();
      return {_id: result._id};
    },
    async updateClass(obj, {_id, input}) {
      const {name, description, semester, course, academicSubject, material} = input;
      const data = db.findById(_id);
      // if (!data) { throw new UpdateUserNotFoundError(); }
      const update = {
        updateDate: new Date(),
        name, description, semester, course, academicSubject, material
      };
      await db.updateOne({_id: _id}, update);
      return  {_id: _id};
    }
  }
};

module.exports = [{resolver}, Class];