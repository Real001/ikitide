const ListMeta = require('../ListMeta');
const ListOptions = require('../ListOptions');

const schema = `
    type User {
    _id: ObjectId!
    lastName: String
    firstName: String
    middleName: String
    email: String
    login: String
    password: String
    role: String
    learnGroup: String
  }
  
  type Users {
    byId(_id: ObjectId!): User
    list(listOptions: ListOptions!, findOptions: UsersFindOptions): UsersList
    byToken(token: String!): User
  }
  
  input UsersFindOptions {
    lastName: String
    firstName: String
    middleName: String
    login: String
  }
  type UsersList {
    meta: ListMeta
    items: [User]
  }
  
  extend type Mutation{
    createUser(input: CreateUserInput!): CreateUserResult
    updateUser(_id:ObjectId input: UpdateUserInput!): CreateUserResult
  }
  type CreateUserResult{
    _id: ObjectId!
  }
  
  input CreateUserInput {
    lastName: String
    firstName: String
    middleName: String
    email: String
    login: String
    password: String
    role: String
    learnGroup: String
  }
  
  input UpdateUserInput {
    lastName: String
    firstName: String
    middleName: String
    email: String
    login: String
    password: String
    role: String
    learnGroup: String
  }
`;

module.exports = [{schema}, ListMeta, ListOptions];