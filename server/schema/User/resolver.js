const {createError} = require('apollo-errors');
const bcrypt = require('bcrypt');
const User = require('./schema');
const db = require('../../models/users');

const UpdateUserNotFoundError = createError('UpdateUserNotFoundError', {
  message: 'Пользователь не найден',
});

const resolver = {
  Users: {
    async byId(obj, {_id}) {
      let user = await db.findOne({_id: _id});
      return user;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.lastName !== 'undefined') {
          filter.lastName = new RegExp(`.*${findOptions.lastName}.*`, 'i');
        }
        if (typeof findOptions.firstName !== 'undefined') {
          filter.firstName = new RegExp(`.*${findOptions.firstName}.*`, 'i');
        }
        if (typeof findOptions.middleName !== 'undefined') {
          filter.middleName = new RegExp(`.*${findOptions.middleName}.*`, 'i');
        }
        if (typeof findOptions.login !== 'undefined') {
          filter.login = new RegExp(`.*${findOptions.login}.*`, 'i');
        }
        if (typeof findOptions.role !== 'undefined') {
          filter.role = findOptions.role;
        }
      }
      const items = db.find(filter).limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    },
    async byToken(obj, {token}) {
      let user = await db.findOne({token});
      return user;
    },
  },
  Mutation: {
    async createUser(obj, {input}) {
      const {lastName, firstName, middleName, email, login, password, learnGroup} = input;
      const user = {lastName, firstName, middleName, email, login, password, learnGroup, role: "Студент"};
      user.salt = await bcrypt.genSalt();
      user.password = await bcrypt.hash(password, user.salt);
      const result = await new db(user).save();
      return {_id: result._id};
    },
    async updateUser(obj, {_id, input}) {
      let {lastName, firstName, middleName, email, login, password, role, learnGroup} = input;
      const data = db.findById(_id);
      if (!data) { throw new UpdateUserNotFoundError(); }
      const salt = await bcrypt.genSalt();
      password = await bcrypt.hash(password, salt);
      const update = {
        updateDate: new Date(),
        lastName, firstName, middleName, email, login, password, role, learnGroup
      };
      await db.updateOne({_id: _id}, update);
      return  {_id: _id};
    }
  }
};
const init = async () => {
  await db.createIndex({login: 1});
};

module.exports = [{resolver}, User, init];