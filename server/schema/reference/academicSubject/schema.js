const ListMeta = require('../../ListMeta');
const ListOptions = require('../../ListOptions');

const schema = `
  type AcademicSubject {
    _id: ObjectId!
    name: String!
    title: String!
  }
  
  input AcademicSubjectInput {
    _id: ObjectId!
    name: String
    title: String
  }
  
   type AcademicSubjects {
    byId(_id: ObjectId!): AcademicSubject
    list(listOptions: ListOptions!, findOptions: AcademicSubjectFindOptions): AcademicSubjectsList
  }
  
  input AcademicSubjectFindOptions {
    name: String
  }
  type AcademicSubjectsList {
    meta: ListMeta
    items: [AcademicSubject]
  }
  
`;

module.exports = [{schema},  ListMeta, ListOptions];