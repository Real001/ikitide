const AcademicSubject = require('./schema');
const db = require('../../../models/academicSubject');

const resolver = {
  AcademicSubjects: {
    async byId(obj, {_id}) {
      let as = await db.findOne({_id: _id});
      return as;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.name !== 'undefined') {
          filter.name = new RegExp(`.*${findOptions.name}.*`, 'i');
        }
      }
      const items = await db.find(filter).limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  }
};

module.exports = [{resolver}, AcademicSubject];