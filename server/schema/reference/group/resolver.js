const Group = require('./schema');
const db = require('../../../models/group');

const resolver = {
  Groups: {
    async byId(obj, {_id}) {
      let group = await db.findOne({_id: _id});
      return group;
    },
    async list(obj, {listOptions, findOptions}) {
      const filter = {};
      if (findOptions) {
        if (typeof findOptions.number !== 'undefined') {
          filter.number = new RegExp(`.*${findOptions.number}.*`, 'i');
        }
        if (typeof findOptions.course !== 'undefined') {
          filter.course = new RegExp(`.*${findOptions.course}.*`, 'i');
        }
        if (typeof findOptions.power !== 'undefined') {
          filter.power = new RegExp(`.*${findOptions.power}.*`, 'i');
        }
      }
      const items = await db.find(filter).limit(listOptions.count);
      const total = await db.count(filter);
      return {items, meta: {total, count: items.count, offset: 0}};
    }
  }
};

module.exports = [{resolver}, Group];