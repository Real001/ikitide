const ListMeta = require('../../ListMeta');
const ListOptions = require('../../ListOptions');

const schema = `
  type Group {
    _id: ObjectId!
    number: String!
    course: Int!
    power: String!
  }
  
  input GroupInput {
    _id: ObjectId!
     number: String
    course: Int
    power: String
  }
  
   type Groups {
    byId(_id: ObjectId!): Group
    list(listOptions: ListOptions!, findOptions: GroupFindOptions): GroupsList
  }
  
  input GroupFindOptions {
    number: String
    course: Int
    power: String
  }
  type GroupsList {
    meta: ListMeta
    items: [Group]
  }
`;

module.exports = [{schema},  ListMeta, ListOptions];