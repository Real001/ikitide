const mongoose = require('mongoose');
const academicSubject = require('./academicSubject');
const classes = require('./classes');

const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  academicSubject: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'academicSubjects',
  },
  description: {
    type: String,
    required: true
  },
  themeClass: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'classes',
    required: true
  },
  inputData: {
    type: String,
    required: true
  },
  outputData: {
    type: String,
    required: true
  },
  time: {
    type: Number,
    required: true
  },
  testData: {
    type: []
  },
  created: {
    type: Date,
    default: Date.now
  },
});

const Task = mongoose.model('tasks', taskSchema);

module.exports = Task;