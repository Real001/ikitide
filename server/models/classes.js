const mongoose = require('mongoose');
const academicSubject = require('./academicSubject');

const classesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  semester: {
    type: Number,
    required: true
  },
  course: {
    type: Number,
    required: true
  },
  academicSubject: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'academicSubjects',
  },
  created: {
    type: Date,
    default: Date.now
  },
  material: {
    type: String,
    required: true
  },
});

const Classes = mongoose.model('classes', classesSchema);

module.exports = Classes;