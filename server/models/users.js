const mongoose = require('mongoose');
const crypto = require('crypto');

const userSchema = new mongoose.Schema({
  lastName: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  middleName: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true
  },
  learnGroup: {
    type: String,
  },
  login: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  password: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  token: {
    type: String,
  }
});

// userSchema.virtual('password')
// .set(function (password) {
//     this._plainPassword = password;
//     if (password) {
//         this.salt = crypto.randomBytes(128).toString('base64')
//         this.passwordHash = crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1')
//     } else {
//         this.salt = undefined;
//         this.passwordHash = undefined;
//     }
// })
//
// .get(function () {
//     return this._plainPassword;
// });
//
// userSchema.methods.checkPassword = function (password) {
//     if (!password) {
//         return false;
//     }
//     if (!this.passwordHash) {
//         return false;
//     }
//     return crypto.pbkd2Sync(password, this.salt, 1, 128, 'sha1') == this.passwordHash;
// };

const User = mongoose.model('User', userSchema);

module.exports = User;