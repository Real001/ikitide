const mongoose = require('mongoose');

const startLessonsSchema = new mongoose.Schema({
  groups: [{
    type: String,

  }],
  academicSubject: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'academicSubjects'
  },
  classes: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'classes'
  },
  created: {
    type: Date,
    default: Date.now
  },
});

const  startLessons = mongoose.model('startLesson', startLessonsSchema);

module.exports =  startLessons;