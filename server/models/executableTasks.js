const mongoose = require('mongoose');

const executableTaskSchema = new mongoose.Schema({
  startLesson: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'startlesson'
  },
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'tasks'
  },
  timeTask: {
    type: String,
  },
  created: {
    type: Date,
    default: Date.now
  },
});

const executableTasks = mongoose.model('executableTasks', executableTaskSchema);

module.exports = executableTasks;