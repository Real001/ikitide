const mongoose = require('mongoose');

const completedTaskSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  lesson: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'classes'
  },
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'tasks'
  },
  test: {
    type: Number,
  },
  code: {
    type: String,
  },
  created: {
    type: Date,
    default: Date.now
  },
});

const completedTasks = mongoose.model('completedTasks', completedTaskSchema);

module.exports = completedTasks;