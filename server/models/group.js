const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
  number: {
    type: String,
    required: true
  },
  course: {
    type: Number,
    required: true
  },
  power: {
    type: String,
    required: true
  },
});

const Groups = mongoose.model('Groups', groupSchema);

module.exports = Groups;