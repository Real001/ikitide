const mongoose = require('mongoose');

const academicSubjectsSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
}, { collection: 'academicSubjects' });

const AcademicSubjects = mongoose.model('academicSubjects', academicSubjectsSchema);

module.exports = AcademicSubjects;