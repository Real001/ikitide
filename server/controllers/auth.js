const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const User = require('../models/users');
const bCrypt = require('bcrypt-nodejs');

module.exports = function (passport) {

    passport.use('login', new LocalStrategy({
        passReqToCallback: true
    }, function (req, login, password, callback) {
        User.findOne({'login': login}, function (err, user) {
            if (err) {
                callback(err);
            }
            if (!user) {
                return callback(null, false, req.flash('message', 'User not found'));
            }
            if (!isValidPassword(user, password)) {
                return callback(null, false, req.flash('message', "Invalid password"));
            }
            return callback(null, callback);

        });
    }));

    passport.use('signup', new LocalStrategy({
        passReqToCallback: true
    }, function (req, login, password, callback) {
        findOrCreateUser = function () {
            User.findOne({'login': login}, function (err, user) {
                if (err) {
                    console.log('Error in SignUp: '+err);
                    return callback(err);
                }
                if (user) {
                    console.log('User already exists with username: '+login);
                    return callback(null, false, req.flash('message', 'User already exists'));
                } else {
                    console.log(req.body);
                    let user = new User(req.body);
                    user.save(function (err) {
                        if (err) {
                            console.log('Error in Saving user: '+err);
                            throw err;
                        }
                        console.log('User Registration succesful');
                        return callback(null, user);
                    });
                }
            });
        };
        process.nextTick(findOrCreateUser);
    }));
    
    passport.serializeUser(function (user, callback) {
        console.log('serializing user: ');console.log(user);
        callback(null, user._id);
    });

    passport.deserializeUser(function (id, callback) {
        User.findById(id, function (err, user) {
            console.log('deserializing user:',user);
            callback(err, user);
        });
    });

    const isValidPassword = function (user, password) {
        return bCrypt.compare(password, user.password);
    };

    const createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };
};



