//server.js
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const log = require('../lib/log')(module);
const path = require('path'); // модуль для парсинга пути
const favicon = require('serve-favicon');
const config = require('../lib/config');
const passport = require('passport');
const flash = require('req-flash');
const {graphqlExpress, graphiqlExpress} = require('apollo-server-express');
const {makeExecutableSchema} = require('graphql-tools');
const {formatError} = require('apollo-errors');
const {mergeResolver, resolveDeps} = require('./until/schema');

const Schema = require('./schema/Root');
const app = express();
require('../lib/mongoose');


app.use(favicon(path.join(__dirname,'../','client','public','images','favicon.ico')));
app.use(morgan('dev'));// log every request to the console
app.use(cookieParser());//чтение cookie файлов(необходимо для авторизации)
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(session({secret: 'SECRET'}));
// Passport:
app.use(passport.initialize());
app.use(passport.session());

const auth = require('./controllers/auth');
auth(passport);

app.use(flash());

app.set( 'views', './client/views');
app.set('view engine', 'ejs');//использование шаблонизатора ejs

 app.use('/', express.static('./client/public'));

//routes ======================================================================================
require('./routes/routes')(app, passport);

const resolvedSchema = resolveDeps(Schema);
const executableSchema = makeExecutableSchema({
  typeDefs: resolvedSchema.schema,
  resolvers: mergeResolver(resolvedSchema.resolver),
});

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: executableSchema }));
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

const PORT = process.env.PORT || config.get('port')

app.listen(PORT, "0.0.0.0", function () {
    console.log(PORT);
    log.info('Server started: http://localhost:' + PORT + '/');
});

app.use(function (req, res, next) {
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({error: 'Not found'});
    return;
});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({error: err.message});
    return;
});