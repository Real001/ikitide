const {reduce, mergeWith, append, memoize, concat, mergeDeepLeft} = require ('ramda');

module.exports.mergeResolver = reduce(mergeDeepLeft, {});

const resolveDeps = memoize(reduce((acc, item) => {
  return Array.isArray(item)
    ? mergeWith(concat)(acc, resolveDeps(item))
    : mergeWith(append, item, acc);
}, {resolver: [], schema: [], init: []}));
module.exports.resolveDeps = resolveDeps;