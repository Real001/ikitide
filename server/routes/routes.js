const main = require('./handlers/main');
// const auth = require('../controllers/auth');
const jwt = require('jsonwebtoken');

const isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/');
};

module.exports = function (app, passport) {

  app.get('/', main.index);
  app.get('/login1', main.login);

  app.post('/signup', passport.authenticate('signup', {
    successRedirect: '/ide',
    failureRedirect: '/login',
    failureFlash: true
  }));
  // app.post('/login', passport.authenticate('login', {
  //     successRedirect: '/ide',
  //     failureRedirect: '/login',
  //     failureFlash : true
  // }));

  // app.get('/ide',function (req, res) {
  //     res.render('ide');
  // });
  // app.get('/ide', function(req, res){
  //     res.render('ide', { user: req.user });
  // });
  app.get('/teacher*', function (req, res) {
    res.render(__dirname + '/ide.ejs');
  });
  app.get('/student*', function (req, res) {
    res.render(__dirname + '/ide.ejs');
  });
  app.get('/login*', function (req, res) {
    res.render(__dirname + '/ide.ejs');
  });
  app.get('/ide*', function (req, res) {
    res.render(__dirname + '/ide.ejs');
  });

};