const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require ('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: "./client/client.js",
  output: {
    path: __dirname + '/client/public/build/',
    publicPath: "build/",
    filename: "bundle.js"
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /public/],
        use: {
          loader: "babel-loader",
          options: {
            plugins: [ 'transform-async-to-generator', 'transform-class-properties'],
            presets: ['env', 'react']
          }
        }
      },
      {
        test: /\.jsx$/,
        exclude: [/node_modules/, /public/],
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ['css-loader']
        })
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ['css-loader','less-loader']
        })
      },
      {
        test: /\.json$/,
        use: {
          loader: "json-loader"
        }
      },
      {
        test: /\.svg$/,
        use: ['svg-url-loader', 'svg-fill-loader']
      },
      {
        test: /\.(png|jpg|gif)$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
    ]
  },
   plugins: [
    new CleanWebpackPlugin([path.join(__dirname, 'client/public/build')]),
    new ExtractTextPlugin('bundle.css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.js',
      minChunks(module, count) {
        const context = module.context;
        return context && context.indexOf('node_modules') >= 0;
      }
    })
  ],
};
