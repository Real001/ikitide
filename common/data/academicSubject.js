module.exports = [
  {
    title: 'Программирование',
    name: 'Programming',
  }, {
    title: 'Организация процесса проектирования программного обеспечения',
    name: 'Organization of software design process',
  }, {
    title: 'Технические аспекты разработки программного обеспечения',
    name: 'Technical aspects of software development',
  }, {
    title: 'Трансляторы',
    name: 'Translators',
  }
];