#IKITIDE (Разработка)
##Подключение к базе
URI задается в 'config.json'

##Миграция БД
Установка migrate
```
npm install -g  migrate-mongo
```
URI для миграции задается в 'config.js'

Создание нового скрипта миграции
```
migrate-mongo create
```
Запуск миграции
```
migrate-mongo  up
```
Откат миграции
```
migrate-mongo  down
```

##Развертывание на Heroku

```
heroku login
```

```
git push heroku master
```